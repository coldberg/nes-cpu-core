#ifndef __FCE_RICOH_IOLOGIC_IMPL_HPP__
#define __FCE_RICOH_IOLOGIC_IMPL_HPP__

#include "fce_ricoh_iologic.hpp"

namespace libfce {


    template<typename _MachineInterface, typename _BusInterface>
    inline ricoh_iologic<_MachineInterface, _BusInterface>::ricoh_iologic (
        _MachineInterface & mi, _BusInterface & bi)
    :   MachineInterface (mi),
        BusInterface (bi) 
    {}

    template<typename _MachineInterface, typename _BusInterface>
    inline bool ricoh_iologic<_MachineInterface, _BusInterface>
        ::cycle (_BusInterface& bus) {
        switch (bus.address ()) {
            case 0x4014:
            {
                if (bus.read ())
                    break;
                auto addr_ = bus.data () << 8;
                _BusInterface dma_bus;
                MachineInterface.master_cycle (dma_bus.init_read (addr_));
                if (MachineInterface.clock () & 1) {
                    MachineInterface.master_cycle (dma_bus.init_read (addr_));
                }
                for (auto i = 0; i < 256u; ++i) {
                    MachineInterface.master_cycle (dma_bus.init_read (addr_  + i));
                    MachineInterface.master_cycle (dma_bus.init_write (0x2004, dma_bus.data ()));
                }
                return true;
            }

        }
        return false;
    }


}

#endif
