#ifndef __FCE_UTILS_HPP__
#define __FCE_UTILS_HPP__

#include <functional>

namespace libfce {

    template <typename _Xtype, typename _Ltype, typename _Rtype>
    auto between (const _Xtype& x, const _Ltype& l, const _Rtype& r) {
        return x >= l && x < r;
    }

    template <typename _Xtype, typename _Ltype, typename _Rtype>
    auto outside (const _Xtype& x, const _Ltype& l, const _Rtype& r) {
        return x < l || x >= r;
    }

    template <typename _Ttype, typename _Utype = typename std::remove_reference<typename std::remove_const<_Ttype>::type>::type>
    const _Utype& as_const (_Ttype&& object) {
        return const_cast<const _Utype&> (object);
    }
    
    struct on_exit {
        template <typename _OnExit>
        on_exit (_OnExit&& onexit):
            OnExit (std::forward<_OnExit> (onexit))
        {}

        ~on_exit () {
            OnExit ();
        }
    private:
        std::function<void ()> OnExit;
    };

}

#endif
