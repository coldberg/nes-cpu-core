#ifndef __MAPPER_HPP__
#define __MAPPER_HPP__

#include <memory>
#include <functional>
#include <string>
#include "fce_mapper_nrom.hpp"

namespace libfce {
   
    template <typename _MasterBusInterface, 
        typename _SlaveBusInterface>
    struct mapper {
        template <typename _CartData>
        void load (_CartData&&);

        bool master_cycle (_MasterBusInterface& bus);       
        bool slave_cycle (_SlaveBusInterface& bus);         
    private:
        struct mapper_interface {
            virtual ~mapper_interface () = default;
            virtual bool master_cycle (_MasterBusInterface&) = 0;
            virtual bool slave_cycle (_SlaveBusInterface&) = 0;
        };

        template <template <typename...> class _MapperType>
        struct mapper_adapter:
            public mapper_interface 
        {
            template <typename... _Args>
            mapper_adapter (_Args&&... args): 
                Mapper (std::forward<_Args> (args)...)
            {}

            bool master_cycle (_MasterBusInterface& bus) override {
                return Mapper.master_cycle (bus);
            }

            bool slave_cycle (_SlaveBusInterface& bus) override {
                return Mapper.slave_cycle (bus);
            }
            
            ~mapper_adapter () = default;

        private:
            _MapperType<
                _MasterBusInterface, 
                _SlaveBusInterface> 
                Mapper;
        };

        template <template <typename...> 
            class _MapperType, typename... _Args>
        static auto make_mapper (_Args&&... args) {
            return std::make_unique<mapper_adapter<_MapperType>> (
                std::forward<_Args> (args)...);
        }

        template <typename _CartData>
        static auto mapper_for_cart (_CartData&& cart) {
            switch (cart.mapper ()) {
            case 0: return make_mapper<libfce::mappers::nrom> (
                std::forward<_CartData> (cart));                
            }
            throw std::runtime_error ("Mapper not implemented : " +
                std::to_string (cart.mapper ()));
        }

        std::unique_ptr<mapper_interface> Mapper;
    };

    template<typename _MasterBusInterface, typename _SlaveBusInterface>
    template <typename _CartData>
    inline void mapper<_MasterBusInterface, _SlaveBusInterface>::load (_CartData&& cart) {
        Mapper = mapper_for_cart (std::forward<_CartData> (cart));
    }

    template<typename _MasterBusInterface, typename _SlaveBusInterface>
    inline bool mapper<_MasterBusInterface, _SlaveBusInterface>::master_cycle (_MasterBusInterface & bus) {        
        return Mapper ? Mapper->master_cycle (bus) : false;
    }

    template<typename _MasterBusInterface, typename _SlaveBusInterface>
    inline bool mapper<_MasterBusInterface, _SlaveBusInterface>::slave_cycle (_SlaveBusInterface & bus) {
        return Mapper ? Mapper->slave_cycle (bus) : false;
    }

}

#endif

