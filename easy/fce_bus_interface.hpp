#ifndef __FCE_BUS_INTERFACE_HPP__
#define __FCE_BUS_INTERFACE_HPP__

#include <cstddef>
#include <cstdint>
#include "fce_bits.hpp"

namespace libfce {
    


    template <bool _BusConflictEnabled = true>
    struct bus_interface {

        auto address () const { return std::uint16_t (Address); }
        auto data () const { return std::uint8_t (Data); }
        auto write () const { return !Read; }
        auto read () const { return !!Read; }
        auto disable () const { return !!Disable; }
        auto nmi () const { return !!Nmi; }
        auto irq () const { return !!Irq; }
        auto reset () const { return !!Reset; }

        auto& address () { return Address; }
        auto& read () { return Read; }
        auto& disable () { return Disable; }
        auto& data () { return Data; }
        auto& nmi () { return Nmi; }
        auto& irq () { return Irq; }        
        auto& reset () { return Reset; }

        auto data (std::uint8_t value_) {
            if (_BusConflictEnabled)
                return std::uint8_t (Data &= value_);
            return std::uint8_t (Data = value_);
        }

        auto nmi (int e) {
            Nmi = e;
        }

        auto irq (int e) {
            Irq = e;
        }

        auto reset (int e) {
            Reset = e;
        }

        bus_interface& init_read (std::uint16_t address_ = 0x0000u) {
            Address = address_;
            Data    = 0xFF;
            Read    = 1;
            Disable = 0;
            Nmi     = 0;
            Reset   = 0;
            Irq     = 0;
            return *this;
        }
        bus_interface& init_write (std::uint16_t address_ = 0x0000u, std::uint8_t data_ = 0x00u) {
            Address = address_;
            Data    = data_;
            Read    = 0;
            Disable = 0;
            Nmi     = 0;
            Reset   = 0;
            Irq     = 0;
            return *this;
        }


    protected:

        template <std::size_t _Offset, std::size_t _Size>
        using register_bits = bit_field<
            _Offset, _Size, std::uint32_t>;
        union {
            std::uint32_t BusState;
            register_bits<0, 16> Address;
            register_bits<0, 8> AddressLow;
            register_bits<8, 8> AddressHigh;
            register_bits<16, 8> Data;
            register_bits<24, 1> Read;
            register_bits<25, 1> Disable;
            register_bits<26, 1> Nmi;
            register_bits<27, 1> Reset;
            register_bits<28, 1> Irq;
        };
    };
}

#endif