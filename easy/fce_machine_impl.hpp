#ifndef __FCE_MACHINE_IMPL_HPP__
#define __FCE_MACHINE_IMPL_HPP__

#include "fce_machine.hpp"
#include "fce_disasm.hpp"

#include <iostream>
#include <iomanip>
#include <sstream>

namespace libfce {

    inline machine::machine ():
        MasterClock (0u),
        RicohCore (*this, MasterBus),
        RicohVideo (*this, SlaveBus),
        RicohIoLogic (*this, MasterBus)
    {}


    template <typename _BusInterface>
    inline bool machine::master_cycle (_BusInterface& bus) {
        ++MasterClock;
        
        auto connected = false;
        connected = MasterRam.cycle (bus)     || connected;
        connected = RicohIoLogic.cycle (bus)  || connected;
        connected = RicohVideo.cycle (bus)    || connected;
        connected = Mapper.master_cycle (bus) || connected;

        if (bus.address () >= 0x2000 && bus.address () < 0x4000) {
        std::cout
            << std::setw (4) << std::setfill ('0') << std::hex << u16 (bus.address ())
            << (bus.write () ? " <- " : " -> ")
            << std::setw (2) << std::setfill ('0') << std::hex << u16 (bus.data ())
            << "\n";
        }
        return connected;
    }

    template <typename _BusInterface>
    inline bool machine::slave_cycle (_BusInterface& bus) {
        auto connected = false;
        connected = Mapper.slave_cycle (bus) || connected;
        connected = SlaveRamMain.cycle (bus) || connected;
        connected = SlaseRamPal.cycle (bus)  || connected;
        return connected;
    }

    template <typename _CartData>
    inline void machine::load (_CartData && cart_) {
        Mapper.load (std::forward<_CartData> (cart_));
    }

    inline void machine::run_for_one_frame () {
        while (MasterClock < 29781u) {
            if (self_test ()) {
                log_operation ();
                log_state ();
            }
            RicohCore.one ();            
        }
        MasterClock = MasterClock - 29781u;
    }

    inline void machine::hard_reset () {
        MasterClock = 0u;
        MasterRam.reset ();
        SlaveRamMain.reset ();
        SlaveRamPal.reset ();
        RicohCore.reset ();
        RicohVideo.reset ();
    }

    inline void machine::soft_reset () {
        RicohCore.reset ();
        RicohVideo.reset ();
    }

    inline void machine::grab_frame (void*) 
    {}

    inline auto machine::clock () const {
        return MasterClock;
    }

    inline bool machine::self_test () const {
        return false;
    }

    template <typename _BusInterface>
    inline bool machine::unclocked_master_cycle (_BusInterface& bus) {
        auto connected = false;
        connected = MasterRam.cycle (bus) || connected;
        connected = Mapper.master_cycle (bus) || connected;
        return connected;
    }

    inline void machine::log_operation () {
        const auto& state_ = RicohCore.state ();
        disassemble ([&] (const std::string& write_) {
            std::cout << write_  << " ";
        }, [&] (std::uint16_t address_) {
            master_bus_type mbus;
            unclocked_master_cycle (mbus.init_read (address_));
            return mbus.data ();
        }, state_.Pc, 1u);

    }

    inline void machine::log_state () {
        auto PixelClock = MasterClock*3;
        std::array<char, 100> buffer_;
        const auto& state_ = RicohCore.state ();
        auto cyc = PixelClock % 341;
        auto sl = PixelClock / 341;
        sl = (sl + 242) % 262 - 1;
        std::sprintf (std::data (buffer_), "A:%02X X:%02X Y:%02X P:%02X SP:%02X CYC:%3lld SL:%lld",
            std::uint8_t (state_.A), std::uint8_t (state_.X), std::uint8_t (state_.Y), std::uint8_t (state_.P),
            std::uint8_t (state_.S), cyc, sl);
        std::cout << std::data (buffer_) << "\n";
    }

}

#endif
