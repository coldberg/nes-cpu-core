#ifndef __FCE_BITS_HPP__
#define __FCE_BITS_HPP__

#include <cstddef>
#include <cstdlib>
#include <type_traits>

namespace libfce {

    typedef std::uint8_t    u8;
    typedef std::uint16_t   u16;
    typedef std::int8_t     i8;
    typedef std::int16_t    i16;


    template <typename _Ttype>
    struct as_type {
        typedef _Ttype type;
    };

    template <std::size_t _Count = 8u>
    struct next_power_of_two: 
        std::conditional_t<(_Count < 64u), 
            next_power_of_two<_Count + 1u>, 
            std::integral_constant<std::size_t, 64u>
        >
    {};

    template <>
    struct next_power_of_two<8u>:
        std::integral_constant<std::size_t, 8u>
    {};

    template <>
    struct next_power_of_two<16u>:
        std::integral_constant<std::size_t, 16u>
    {};

    template <>
    struct next_power_of_two<32u>:
        std::integral_constant<std::size_t, 32u>
    {};

    template <>
    struct next_power_of_two<64u>:
        std::integral_constant<std::size_t, 64u>
    {};

    template <std::size_t _Bits = 8, bool _Signed = false>
    struct type_of_size: type_of_size<next_power_of_two<_Bits>::value, _Signed> {};

    template <> struct type_of_size<8, true>: as_type<std::int8_t> {};
    template <> struct type_of_size<16, true>: as_type<std::int16_t> {};
    template <> struct type_of_size<32, true>: as_type<std::int32_t> {};
    template <> struct type_of_size<64, true>: as_type<std::int64_t> {};
    template <> struct type_of_size<8, false>: as_type<std::uint8_t> {};
    template <> struct type_of_size<16, false>: as_type<std::uint16_t> {};
    template <> struct type_of_size<32, false>: as_type<std::uint32_t> {};
    template <> struct type_of_size<64, false>: as_type<std::uint64_t> {};

    template <std::size_t _Offset = 0u, std::size_t _Size = 0u,
        typename _Ttype = typename type_of_size<_Offset+_Size>::type>
    struct bit_field {
        typedef typename type_of_size<_Size>::type _Utype;

        static const _Ttype mask = (_Ttype (1) << _Size) - _Ttype (1);
        static const _Ttype smask = mask << _Offset;

        bit_field& store (_Utype value_);
        _Utype load () const;        
        bit_field& operator = (_Utype value_);
        operator _Utype () const;        
        bit_field& operator += (_Utype delta_);        
        bit_field& operator -= (_Utype delta_);
        bit_field& operator |= (_Utype delta_);
        bit_field& operator &= (_Utype delta_);
        bit_field& operator ^= (_Utype delta_);
        bit_field& operator ++ ();
        bit_field& operator -- ();
        _Utype operator ++ (int);
        _Utype operator -- (int);        
    private:
        _Ttype data_;
    };

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>    
    inline bit_field<_Offset, _Size, _Ttype>& 
        bit_field<_Offset, _Size, _Ttype>::operator = (_Utype value_) 
    {
        return store (value_);    
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline bit_field<_Offset, _Size, _Ttype>::operator _Utype () const {
        return load ();
    }


    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>    
    inline bit_field<_Offset, _Size, _Ttype>&        
        bit_field<_Offset, _Size, _Ttype>::store (_Utype value_) 
    {
        data_ = (data_ & ~smask) | ((_Ttype (_Size > 1 ?
            value_ & mask : !!value_) & mask) << _Offset);
        return *this;
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline typename bit_field<_Offset, _Size, _Ttype>::_Utype
        bit_field<_Offset, _Size, _Ttype>::load () const 
    {
        return _Utype ((data_ >> _Offset) & mask);
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>    
    inline bit_field<_Offset, _Size, _Ttype>& 
        bit_field<_Offset, _Size, _Ttype>::operator += (_Utype delta_)
    {
        return store (load () + delta_);
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>    
    inline bit_field<_Offset, _Size, _Ttype>& 
        bit_field<_Offset, _Size, _Ttype>::operator -= (_Utype delta_)
    {
        return store (load () - delta_);
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline bit_field<_Offset, _Size, _Ttype>&
        bit_field<_Offset, _Size, _Ttype>::operator |= (_Utype delta_) {
        return store (load () | delta_);
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline bit_field<_Offset, _Size, _Ttype>&
        bit_field<_Offset, _Size, _Ttype>::operator &= (_Utype delta_) {
        return store (load () &  delta_);
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline bit_field<_Offset, _Size, _Ttype>&
        bit_field<_Offset, _Size, _Ttype>::operator ^= (_Utype delta_) {
        return store (load () ^ delta_);
    }


    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline bit_field<_Offset, _Size, _Ttype>&
        bit_field<_Offset, _Size, _Ttype>::operator ++ ()
    {
        return *this += 1;
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline bit_field<_Offset, _Size, _Ttype>&
        bit_field<_Offset, _Size, _Ttype>::operator -- ()
    {
        return *this -= 1;
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline typename bit_field<_Offset, _Size, _Ttype>::_Utype
        bit_field<_Offset, _Size, _Ttype>::operator ++ (int) 
    {
        auto temp = load ();
        ++*this;
        return temp;
    }

    template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    inline typename bit_field<_Offset, _Size, _Ttype>::_Utype
        bit_field<_Offset, _Size, _Ttype>::operator -- (int) 
    {
        auto temp = load ();
        --*this;
        return temp;
    }

    //template <std::size_t _Offset, std::size_t _Size, typename _Ttype>
    //inline bit_field<_Offset, _Size, _Ttype>::operator bool () const {
    //    return !!load ();
    //}

}

#endif