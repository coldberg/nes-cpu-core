#ifndef __FCE_MAPPER_NROM_HPP__
#define __FCE_MAPPER_NROM_HPP__

#include <cstddef>
#include <cstdint>
#include <array>
#include "fce_utils.hpp"

namespace libfce {
    namespace mappers {
        template <typename _MasterBusInterface, 
            typename _SlaveBusInterface>
        struct nrom {
            template <typename _Cart>
            nrom (_Cart&& cart_):
                //Prg (std::begin (cart_.prg ()), std::end (cart_.prg ())),
                //Chr (std::begin (cart_.chr ()), std::end (cart_.chr ())),
                PrgAddressMask (cart_.prg ().size () - 1u),
                ChrAddressMask (cart_.chr ().size () - 1u),
                RamAddressMask (cart_.ram_size () ? cart_.ram_size () - 1u : 0)
            {
                std::copy (std::begin (cart_.prg ()), std::end (cart_.prg ()), std::begin (Prg));
                std::copy (std::begin (cart_.chr ()), std::end (cart_.chr ()), std::begin (Chr));
            }

            bool master_cycle (_MasterBusInterface& bus) {
                auto address_ = bus.address ();
                if (address_ < 0x6000)
                    return false;
                if (address_ < 0x8000) {
                    auto& cell_ = Ram [(address_ - 0x6000) & RamAddressMask];
                    if (bus.write ()) {
                        cell_ = bus.data ();
                        return true;
                    }
                    bus.data (cell_);
                    return true;
                }
                if (bus.write ())
                    return true;
                bus.data (Prg [(address_ - 0x8000) & PrgAddressMask]);
                return true;
            }

            bool slave_cycle (_SlaveBusInterface& bus) {                
                auto& cell_ = Chr [bus.address () & ChrAddressMask];
                if (bus.write ()) {
                    cell_ = bus.data ();
                    return true;                
                }
                bus.data (cell_);
                return true;
            }

        private:
            std::size_t PrgAddressMask;
            std::size_t ChrAddressMask;
            std::size_t RamAddressMask;

            std::array<std::uint8_t, 0x8000> Prg;
            std::array<std::uint8_t, 0x2000> Chr;
            std::array<std::uint8_t, 0x1000> Ram;
        };
    }
}

#endif