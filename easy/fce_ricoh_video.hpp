#ifndef __FCE_RICOH_VIDEO_HPP__
#define __FCE_RICOH_VIDEO_HPP__

namespace libfce {

    template <typename _MachineInterface, typename _BusInterface>
    struct ricoh_video {
        ricoh_video (_MachineInterface&, _BusInterface&);
        void reset ();
        template <typename _SlaveBusInterface>
        bool cycle (_SlaveBusInterface& bus);

    protected:
        static const auto _VerticalLines = 242u;
        static const auto _HorizontalDots = 341u;
        static const auto _VerticalBlank = 20u;
        static const auto _PostRenderLines = 1u;

        template <typename _ClockCounter>
        void tick (const _ClockCounter& clock);

    private:   

        std::uint8_t ScrollX, ScrollY;
        std::uint8_t VramAddrLow, VramAddrHigh;


        std::array<u8, 0x100> OamData;


        _MachineInterface& MachineInterface;        
        _BusInterface& BusInterface;
    };

}

#include "fce_ricoh_video_impl.hpp"

#endif
