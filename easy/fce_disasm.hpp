#ifndef __INES_DISASM_HPP__
#define __INES_DISASM_HPP__

#include <cstdint>
#include <cstdlib>
#include <string>
#include <unordered_map>
#include <cstdio>
#include <array>

namespace libfce {

    template <typename _Otype, typename _Itype, typename _Address, typename _Count>
    auto disassemble (_Otype& write_, const _Itype& read_, 
        const _Address& addr_, const _Count& num_) 
    try {
      
        static const auto am_acc = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            return "A";
        };

        static const auto am_imp = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            return "";
        };

        static const auto am_imm = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "#$%02X", a);
            return std::data (buffer_);
        }; 

        static const auto am_zpg = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "$%02X", a);
            return std::data (buffer_);
        };

        static const auto am_zpx = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "$%02X,X", a);
            return std::data (buffer_);
        };

        static const auto am_zpy = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "$%02X,Y", a);
            return std::data (buffer_);
        };

        static const auto am_abs = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "$%04X", (b<<8)|a);
            return std::data (buffer_);
        };

        static const auto am_abx = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "$%04X,X", (b<<8)|a);
            return std::data (buffer_);
        };

        static const auto am_aby = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "$%04X,Y", (b<<8)|a);
            return std::data (buffer_);
        };

        static const auto am_ind = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "($%04X)", (b<<8)|a);
            return std::data (buffer_);
        };

        static const auto am_inx = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "($%02X,X)", a);
            return std::data (buffer_);
        };

        static const auto am_iny = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "($%02X),Y", a);
            return std::data (buffer_);
        };

        static const auto am_rel = [] (std::uint8_t a, std::uint8_t b, std::uint16_t c) -> std::string {
            std::array<char, 100> buffer_;
            std::sprintf (std::data (buffer_), "$%04X", std::uint16_t (c + 2 + std::int8_t (a)));
            return std::data (buffer_);
        };
      
        typedef std::string format_t (std::uint8_t, std::uint8_t, std::uint16_t);

        struct instruction_template: 
            std::tuple<std::string, std::size_t, format_t*> 
        {            
            instruction_template (const char* name, std::size_t size, format_t* fun):
                std::tuple<std::string, std::size_t, format_t*> (name, size, fun)
            {}

            decltype (auto) name () const { return std::get<0> (*this); }
            decltype (auto) size () const { return std::get<1> (*this); }
            decltype (auto) format () const { return std::get<2> (*this); }

            std::string operator () (std::uint8_t a, std::uint8_t b, std::uint16_t c) const {
                return format () (a, b, c);
            }
        };

        static const auto instruction_map_ = 
            std::unordered_map<std::uint8_t, instruction_template> 
        {
            {std::uint8_t (0x69), instruction_template ("ADC", 2, am_imm)},
            {std::uint8_t (0x65), instruction_template ("ADC", 2, am_zpg)},
            {std::uint8_t (0x75), instruction_template ("ADC", 2, am_zpx)},
            {std::uint8_t (0x6D), instruction_template ("ADC", 3, am_abs)},
            {std::uint8_t (0x7D), instruction_template ("ADC", 3, am_abx)},
            {std::uint8_t (0x79), instruction_template ("ADC", 3, am_aby)},
            {std::uint8_t (0x61), instruction_template ("ADC", 2, am_inx)},
            {std::uint8_t (0x71), instruction_template ("ADC", 2, am_iny)},
            {std::uint8_t (0x29), instruction_template ("AND", 2, am_imm)},
            {std::uint8_t (0x25), instruction_template ("AND", 2, am_zpg)},
            {std::uint8_t (0x35), instruction_template ("AND", 2, am_zpx)},
            {std::uint8_t (0x2D), instruction_template ("AND", 3, am_abs)},
            {std::uint8_t (0x3D), instruction_template ("AND", 3, am_abx)},
            {std::uint8_t (0x39), instruction_template ("AND", 3, am_aby)},
            {std::uint8_t (0x21), instruction_template ("AND", 2, am_inx)},
            {std::uint8_t (0x31), instruction_template ("AND", 2, am_iny)},
            {std::uint8_t (0x0A), instruction_template ("ASL", 1, am_acc)},
            {std::uint8_t (0x06), instruction_template ("ASL", 2, am_zpg)},
            {std::uint8_t (0x16), instruction_template ("ASL", 2, am_zpx)},
            {std::uint8_t (0x0E), instruction_template ("ASL", 3, am_abs)},
            {std::uint8_t (0x1E), instruction_template ("ASL", 3, am_abx)},
            {std::uint8_t (0xF0), instruction_template ("BEQ", 2, am_rel)},
            {std::uint8_t (0xD0), instruction_template ("BNE", 2, am_rel)},
            {std::uint8_t (0x90), instruction_template ("BCC", 2, am_rel)},
            {std::uint8_t (0xB0), instruction_template ("BCS", 2, am_rel)},
            {std::uint8_t (0x30), instruction_template ("BMI", 2, am_rel)},
            {std::uint8_t (0x10), instruction_template ("BPL", 2, am_rel)},
            {std::uint8_t (0x50), instruction_template ("BVC", 2, am_rel)},
            {std::uint8_t (0x70), instruction_template ("BVS", 2, am_rel)},
            {std::uint8_t (0x24), instruction_template ("BIT", 2, am_zpg)},
            {std::uint8_t (0x2C), instruction_template ("BIT", 3, am_abs)},
            {std::uint8_t (0x00), instruction_template ("BRK", 1, am_imp)},
            {std::uint8_t (0x18), instruction_template ("CLC", 1, am_imp)},
            {std::uint8_t (0xD8), instruction_template ("CLD", 1, am_imp)},
            {std::uint8_t (0x58), instruction_template ("CLI", 1, am_imp)},
            {std::uint8_t (0xB8), instruction_template ("CLV", 1, am_imp)},
            {std::uint8_t (0xC9), instruction_template ("CMP", 2, am_imm)},
            {std::uint8_t (0xC5), instruction_template ("CMP", 2, am_zpg)},
            {std::uint8_t (0xD5), instruction_template ("CMP", 2, am_zpx)},
            {std::uint8_t (0xCD), instruction_template ("CMP", 3, am_abs)},
            {std::uint8_t (0xDD), instruction_template ("CMP", 3, am_abx)},
            {std::uint8_t (0xD9), instruction_template ("CMP", 3, am_aby)},
            {std::uint8_t (0xC1), instruction_template ("CMP", 2, am_inx)},
            {std::uint8_t (0xD1), instruction_template ("CMP", 2, am_iny)},
            {std::uint8_t (0xE0), instruction_template ("CPX", 2, am_imm)},
            {std::uint8_t (0xE4), instruction_template ("CPX", 2, am_zpg)},
            {std::uint8_t (0xEC), instruction_template ("CPX", 3, am_abs)},
            {std::uint8_t (0xC0), instruction_template ("CPY", 2, am_imm)},
            {std::uint8_t (0xC4), instruction_template ("CPY", 2, am_zpg)},
            {std::uint8_t (0xCC), instruction_template ("CPY", 2, am_abs)},
            {std::uint8_t (0xC6), instruction_template ("DEC", 2, am_zpg)},
            {std::uint8_t (0xD6), instruction_template ("DEC", 2, am_zpx)},
            {std::uint8_t (0xCE), instruction_template ("DEC", 3, am_abs)},
            {std::uint8_t (0xDE), instruction_template ("DEC", 3, am_abx)},
            {std::uint8_t (0xCA), instruction_template ("DEX", 1, am_imp)},
            {std::uint8_t (0x88), instruction_template ("DEY", 1, am_imp)},
            {std::uint8_t (0x49), instruction_template ("EOR", 2, am_imm)},
            {std::uint8_t (0x45), instruction_template ("EOR", 2, am_zpg)},
            {std::uint8_t (0x55), instruction_template ("EOR", 2, am_zpx)},
            {std::uint8_t (0x4D), instruction_template ("EOR", 3, am_abs)},
            {std::uint8_t (0x5D), instruction_template ("EOR", 3, am_abx)},
            {std::uint8_t (0x59), instruction_template ("EOR", 3, am_aby)},
            {std::uint8_t (0x41), instruction_template ("EOR", 2, am_inx)},
            {std::uint8_t (0x51), instruction_template ("EOR", 2, am_iny)},
            {std::uint8_t (0xE6), instruction_template ("INC", 2, am_zpg)},
            {std::uint8_t (0xF6), instruction_template ("INC", 2, am_zpx)},
            {std::uint8_t (0xEE), instruction_template ("INC", 3, am_abs)},
            {std::uint8_t (0xFE), instruction_template ("INC", 3, am_abx)},
            {std::uint8_t (0xE8), instruction_template ("INX", 1, am_imp)},
            {std::uint8_t (0xC8), instruction_template ("INY", 1, am_imp)},
            {std::uint8_t (0x4C), instruction_template ("JMP", 3, am_abs)},
            {std::uint8_t (0x6C), instruction_template ("JMP", 3, am_ind)},
            {std::uint8_t (0x20), instruction_template ("JSR", 3, am_abs)},
            {std::uint8_t (0xA9), instruction_template ("LDA", 2, am_imm)},
            {std::uint8_t (0xA5), instruction_template ("LDA", 2, am_zpg)},
            {std::uint8_t (0xB5), instruction_template ("LDA", 2, am_zpx)},
            {std::uint8_t (0xAD), instruction_template ("LDA", 3, am_abs)},
            {std::uint8_t (0xBD), instruction_template ("LDA", 3, am_abx)},
            {std::uint8_t (0xB9), instruction_template ("LDA", 3, am_aby)},
            {std::uint8_t (0xA1), instruction_template ("LDA", 2, am_inx)},
            {std::uint8_t (0xB1), instruction_template ("LDA", 2, am_iny)},
            {std::uint8_t (0xA2), instruction_template ("LDX", 2, am_imm)},
            {std::uint8_t (0xA6), instruction_template ("LDX", 2, am_zpg)},
            {std::uint8_t (0xB6), instruction_template ("LDX", 2, am_zpy)},
            {std::uint8_t (0xAE), instruction_template ("LDX", 3, am_abs)},
            {std::uint8_t (0xBE), instruction_template ("LDX", 3, am_aby)},
            {std::uint8_t (0xA0), instruction_template ("LDY", 2, am_imm)},
            {std::uint8_t (0xA4), instruction_template ("LDY", 2, am_zpg)},
            {std::uint8_t (0xB4), instruction_template ("LDY", 2, am_zpx)},
            {std::uint8_t (0xAC), instruction_template ("LDY", 3, am_abs)},
            {std::uint8_t (0xBC), instruction_template ("LDY", 3, am_abx)},
            {std::uint8_t (0x4A), instruction_template ("LSR", 1, am_acc)},
            {std::uint8_t (0x46), instruction_template ("LSR", 2, am_zpg)},
            {std::uint8_t (0x56), instruction_template ("LSR", 2, am_zpx)},
            {std::uint8_t (0x4E), instruction_template ("LSR", 3, am_abs)},
            {std::uint8_t (0x5E), instruction_template ("LSR", 3, am_abx)},
            {std::uint8_t (0xEA), instruction_template ("NOP", 1, am_imp)},
            {std::uint8_t (0x09), instruction_template ("ORA", 2, am_imm)},
            {std::uint8_t (0x05), instruction_template ("ORA", 2, am_zpg)},
            {std::uint8_t (0x15), instruction_template ("ORA", 2, am_zpx)},
            {std::uint8_t (0x0D), instruction_template ("ORA", 3, am_abs)},
            {std::uint8_t (0x1D), instruction_template ("ORA", 3, am_abx)},
            {std::uint8_t (0x19), instruction_template ("ORA", 3, am_aby)},
            {std::uint8_t (0x01), instruction_template ("ORA", 2, am_inx)},
            {std::uint8_t (0x11), instruction_template ("ORA", 2, am_iny)},
            {std::uint8_t (0x48), instruction_template ("PHA", 1, am_imp)},
            {std::uint8_t (0x08), instruction_template ("PHP", 1, am_imp)},
            {std::uint8_t (0x68), instruction_template ("PLA", 1, am_imp)},
            {std::uint8_t (0x28), instruction_template ("PLP", 1, am_imp)},
            {std::uint8_t (0x2A), instruction_template ("ROL", 1, am_acc)},
            {std::uint8_t (0x26), instruction_template ("ROL", 2, am_zpg)},
            {std::uint8_t (0x36), instruction_template ("ROL", 2, am_zpx)},
            {std::uint8_t (0x2E), instruction_template ("ROL", 3, am_abs)},
            {std::uint8_t (0x3E), instruction_template ("ROL", 3, am_abx)},
            {std::uint8_t (0x6A), instruction_template ("ROR", 1, am_acc)},
            {std::uint8_t (0x66), instruction_template ("ROR", 2, am_zpg)},
            {std::uint8_t (0x76), instruction_template ("ROR", 2, am_zpx)},
            {std::uint8_t (0x6E), instruction_template ("ROR", 3, am_abs)},
            {std::uint8_t (0x7E), instruction_template ("ROR", 3, am_abx)},
            {std::uint8_t (0x40), instruction_template ("RTI", 1, am_imp)},
            {std::uint8_t (0x60), instruction_template ("RTS", 1, am_imp)},
            {std::uint8_t (0xE9), instruction_template ("SBC", 2, am_imm)},
            {std::uint8_t (0xEB), instruction_template ("SBC", 2, am_imm)},
            {std::uint8_t (0xE5), instruction_template ("SBC", 2, am_zpg)},
            {std::uint8_t (0xF5), instruction_template ("SBC", 2, am_zpx)},
            {std::uint8_t (0xED), instruction_template ("SBC", 3, am_abs)},
            {std::uint8_t (0xFD), instruction_template ("SBC", 3, am_abx)},
            {std::uint8_t (0xF9), instruction_template ("SBC", 3, am_aby)},
            {std::uint8_t (0xE1), instruction_template ("SBC", 2, am_inx)},
            {std::uint8_t (0xF1), instruction_template ("SBC", 2, am_iny)},
            {std::uint8_t (0x38), instruction_template ("SEC", 1, am_imp)},
            {std::uint8_t (0xF8), instruction_template ("SED", 1, am_imp)},
            {std::uint8_t (0x78), instruction_template ("SEI", 1, am_imp)},
            {std::uint8_t (0x85), instruction_template ("STA", 2, am_zpg)},
            {std::uint8_t (0x95), instruction_template ("STA", 2, am_zpx)},
            {std::uint8_t (0x8D), instruction_template ("STA", 3, am_abs)},
            {std::uint8_t (0x9D), instruction_template ("STA", 3, am_abx)},
            {std::uint8_t (0x99), instruction_template ("STA", 3, am_aby)},
            {std::uint8_t (0x81), instruction_template ("STA", 2, am_inx)},
            {std::uint8_t (0x91), instruction_template ("STA", 2, am_iny)},
            {std::uint8_t (0x86), instruction_template ("STX", 2, am_zpg)},
            {std::uint8_t (0x96), instruction_template ("STX", 2, am_zpy)},
            {std::uint8_t (0x8E), instruction_template ("STX", 3, am_abs)},
            {std::uint8_t (0x84), instruction_template ("STY", 2, am_zpg)},
            {std::uint8_t (0x94), instruction_template ("STY", 2, am_zpy)},
            {std::uint8_t (0x8C), instruction_template ("STY", 3, am_abs)},
            {std::uint8_t (0xAA), instruction_template ("TAX", 1, am_imp)},
            {std::uint8_t (0xA8), instruction_template ("TAY", 1, am_imp)},
            {std::uint8_t (0x8A), instruction_template ("TXA", 1, am_imp)},
            {std::uint8_t (0x9A), instruction_template ("TXS", 1, am_imp)},
            {std::uint8_t (0x98), instruction_template ("TYA", 1, am_imp)},
            {std::uint8_t (0xBA), instruction_template ("TSX", 1, am_imp)},
            
            {std::uint8_t (0x80), instruction_template ("NOP", 2, am_imm)},
            {std::uint8_t (0x82), instruction_template ("NOP", 2, am_imm)},
            {std::uint8_t (0x89), instruction_template ("NOP", 2, am_imm)},
            {std::uint8_t (0xC2), instruction_template ("NOP", 2, am_imm)},
            {std::uint8_t (0xE2), instruction_template ("NOP", 2, am_imm)},
            {std::uint8_t (0x04), instruction_template ("NOP", 3, am_zpg)},
            {std::uint8_t (0x64), instruction_template ("NOP", 3, am_zpg)},
            {std::uint8_t (0x44), instruction_template ("NOP", 3, am_zpg)},
            {std::uint8_t (0x14), instruction_template ("NOP", 2, am_zpx)},
            {std::uint8_t (0x34), instruction_template ("NOP", 2, am_zpx)},
            {std::uint8_t (0x54), instruction_template ("NOP", 2, am_zpx)},
            {std::uint8_t (0x74), instruction_template ("NOP", 2, am_zpx)},
            {std::uint8_t (0xD4), instruction_template ("NOP", 2, am_zpx)},
            {std::uint8_t (0xF4), instruction_template ("NOP", 2, am_zpx)},
            
            {std::uint8_t (0x0C), instruction_template ("NOP", 3, am_abs)},
            {std::uint8_t (0x1C), instruction_template ("NOP", 3, am_abx)},
            {std::uint8_t (0x3C), instruction_template ("NOP", 3, am_abx)},
            {std::uint8_t (0x5C), instruction_template ("NOP", 3, am_abx)},
            {std::uint8_t (0x7C), instruction_template ("NOP", 3, am_abx)},
            {std::uint8_t (0xDC), instruction_template ("NOP", 3, am_abx)},
            {std::uint8_t (0xFC), instruction_template ("NOP", 3, am_abx)},

            {std::uint8_t (0x1A), instruction_template ("NOP", 1, am_imp)}, 
            {std::uint8_t (0x3A), instruction_template ("NOP", 1, am_imp)}, 
            {std::uint8_t (0x5A), instruction_template ("NOP", 1, am_imp)}, 
            {std::uint8_t (0x7A), instruction_template ("NOP", 1, am_imp)}, 
            {std::uint8_t (0xDA), instruction_template ("NOP", 1, am_imp)}, 
            {std::uint8_t (0xFA), instruction_template ("NOP", 1, am_imp)}, 

            {std::uint8_t (0xA7), instruction_template ("LAX", 2, am_zpg)},
            {std::uint8_t (0xB7), instruction_template ("LAX", 2, am_zpy)},
            {std::uint8_t (0xAF), instruction_template ("LAX", 3, am_abs)},
            {std::uint8_t (0xBF), instruction_template ("LAX", 3, am_aby)},
            {std::uint8_t (0xA3), instruction_template ("LAX", 2, am_inx)},
            {std::uint8_t (0xB3), instruction_template ("LAX", 2, am_iny)},

            {std::uint8_t (0x87), instruction_template ("SAX", 2, am_zpg)},
            {std::uint8_t (0x97), instruction_template ("SAX", 2, am_zpy)},
            {std::uint8_t (0x83), instruction_template ("SAX", 2, am_inx)},
            {std::uint8_t (0x8F), instruction_template ("SAX", 3, am_abs)},

            {std::uint8_t (0xC7), instruction_template ("DCP", 2, am_zpg)},
            {std::uint8_t (0xD7), instruction_template ("DCP", 2, am_zpx)},
            {std::uint8_t (0xCF), instruction_template ("DCP", 3, am_abs)},
            {std::uint8_t (0xDF), instruction_template ("DCP", 3, am_abx)},
            {std::uint8_t (0xDB), instruction_template ("DCP", 3, am_aby)},
            {std::uint8_t (0xC3), instruction_template ("DCP", 2, am_inx)},
            {std::uint8_t (0xD3), instruction_template ("DCP", 2, am_iny)},

            {std::uint8_t (0xE7), instruction_template ("ISB", 2, am_zpg)},
            {std::uint8_t (0xF7), instruction_template ("ISB", 2, am_zpx)},
            {std::uint8_t (0xEF), instruction_template ("ISB", 3, am_abs)},
            {std::uint8_t (0xFF), instruction_template ("ISB", 3, am_abx)},
            {std::uint8_t (0xFB), instruction_template ("ISB", 3, am_aby)},
            {std::uint8_t (0xE3), instruction_template ("ISB", 2, am_inx)},
            {std::uint8_t (0xF3), instruction_template ("ISB", 2, am_iny)},

            {std::uint8_t (0x07), instruction_template ("SLO", 2, am_zpg)},
            {std::uint8_t (0x17), instruction_template ("SLO", 2, am_zpx)},
            {std::uint8_t (0x0F), instruction_template ("SLO", 3, am_abs)},
            {std::uint8_t (0x1F), instruction_template ("SLO", 3, am_abx)},
            {std::uint8_t (0x1B), instruction_template ("SLO", 3, am_aby)},
            {std::uint8_t (0x03), instruction_template ("SLO", 2, am_inx)},
            {std::uint8_t (0x13), instruction_template ("SLO", 2, am_iny)},

            {std::uint8_t (0x27), instruction_template ("RLA", 2, am_zpg)},
            {std::uint8_t (0x37), instruction_template ("RLA", 2, am_zpx)},
            {std::uint8_t (0x2F), instruction_template ("RLA", 3, am_abs)},
            {std::uint8_t (0x3F), instruction_template ("RLA", 3, am_abx)},
            {std::uint8_t (0x3B), instruction_template ("RLA", 3, am_aby)},
            {std::uint8_t (0x23), instruction_template ("RLA", 2, am_inx)},
            {std::uint8_t (0x33), instruction_template ("RLA", 2, am_iny)},

            {std::uint8_t (0x47), instruction_template ("SRE", 2, am_zpg)},
            {std::uint8_t (0x57), instruction_template ("SRE", 2, am_zpx)},
            {std::uint8_t (0x4F), instruction_template ("SRE", 3, am_abs)},
            {std::uint8_t (0x5F), instruction_template ("SRE", 3, am_abx)},
            {std::uint8_t (0x5B), instruction_template ("SRE", 3, am_aby)},
            {std::uint8_t (0x43), instruction_template ("SRE", 2, am_inx)},
            {std::uint8_t (0x53), instruction_template ("SRE", 2, am_iny)},

            {std::uint8_t (0x67), instruction_template ("RRA", 2, am_zpg)},
            {std::uint8_t (0x77), instruction_template ("RRA", 2, am_zpx)},
            {std::uint8_t (0x6F), instruction_template ("RRA", 3, am_abs)},
            {std::uint8_t (0x7F), instruction_template ("RRA", 3, am_abx)},
            {std::uint8_t (0x7B), instruction_template ("RRA", 3, am_aby)},
            {std::uint8_t (0x63), instruction_template ("RRA", 2, am_inx)},
            {std::uint8_t (0x73), instruction_template ("RRA", 2, am_iny)}
                
        };

        std::array<char, 100> buffer_;
        std::fill (std::begin (buffer_), std::end (buffer_), ' ');
        auto caddr_ = std::uint16_t (addr_);

        for (auto i = _Count (); i < num_; ++i) {
            std::uint8_t o = read_ (caddr_);
            std::uint8_t a = 0, b = 0;
            try {
                instruction_map_.at (o);
            }
            catch (const std::exception&) {
                char buffer [100];
                std::sprintf (buffer, "Bad opcode $%02X at address $%04X", o, caddr_);
                throw std::runtime_error (buffer);
            }
            const auto& temp_ = instruction_map_.at (o);

            auto s = temp_.name ();
            switch (temp_.size ()) {
                case 1: 
                    std::sprintf (std::data (buffer_), "%04X  %02X        %s ", 
                        caddr_, o, temp_.name ().c_str ());
                    break;
                case 2: 
                    a = read_ (caddr_+1);
                    std::sprintf (std::data (buffer_), "%04X  %02X %02X     %s ", 
                        caddr_, o, a, temp_.name ().c_str ());
                    break;
                case 3: 
                    a = read_ (caddr_+1);
                    b = read_ (caddr_+2);
                    std::sprintf (std::data (buffer_), "%04X  %02X %02X %02X  %s ", 
                        caddr_, o, a, b, temp_.name ().c_str ());
                    break;            
            }

            auto saddr_ = temp_.format () (a, b, caddr_);

            std::sprintf (std::data (buffer_) + 20, "%-26s ", saddr_.c_str ());

            caddr_ = static_cast<std::uint16_t> (caddr_ + temp_.size ());
            write_ (std::string (std::data (buffer_)));
        }
    }
    catch (const std::exception& e) {
        using namespace std::string_literals;
        write_ (std::string ("Can't disassemble line : "s+e.what ()));
    }
    

}


#endif
