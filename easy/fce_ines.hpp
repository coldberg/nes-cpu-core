#ifndef __FCE_INES_HPP__
#define __FCE_INES_HPP__

#include <cstdint>
#include <cstddef>
#include <vector>

namespace libfce {

    struct ines {       
        static ines load (const std::string&);

        auto mapper () const { 
            return m_mapper; 
        }

        const auto& prg () const {
            return m_prg;
        }

        const auto& chr () const {
            return m_chr;
        }

        auto ram_size () const {
            return 0u;
        }

    private:
        std::uint8_t m_mapper;

        std::vector<std::uint8_t> m_prg;
        std::vector<std::uint8_t> m_chr;        
    };

}

#endif