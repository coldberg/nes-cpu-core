#include "fce_ines.hpp"
#include <fstream>
#include <array>


libfce::ines libfce::ines::load (const std::string& path_) {
    auto cart_ = ines ();

    std::ifstream dfile_ (path_, std::ios::binary);
    if (!dfile_) {
        throw std::runtime_error ("Can't open file");
    }
    dfile_.seekg (0, std::ios::end);
    auto size_ = std::uint64_t (dfile_.tellg ());
    dfile_.seekg (0, std::ios::beg);
    if (size_ < 16) {
        throw std::runtime_error ("iNES header missing");
    }

    std::array<char, 16> header_;
    dfile_.read (std::data (header_), 16);

    static const char signature_ [] = {'N', 'E', 'S', 0x1A};

    if (!std::equal (std::begin (header_), std::begin (header_)+4u, std::begin (signature_)))        
        throw std::runtime_error ("Bad iNES signature");        

    cart_.m_mapper = (header_ [6] >> 4) | (header_ [7] & 0xF0);

    auto prg_size = std::size_t (header_ [4]) << 14;
    auto chr_size = std::size_t (header_ [5]) << 13;
    auto trn_size = (header_ [6] & 0x4 ? 1u : 0u) << 5;
    
    if (size_ < 16u+prg_size+chr_size+trn_size)
        throw std::runtime_error ("Bad iNES structure");

    dfile_.seekg (trn_size, std::ios::cur);

    cart_.m_prg.resize (prg_size);
    cart_.m_chr.resize (chr_size);
    dfile_.read (reinterpret_cast<char*>(std::data (cart_.m_prg)), prg_size);
    dfile_.read (reinterpret_cast<char*>(std::data (cart_.m_chr)), chr_size);

    return cart_;
}
