﻿// Example program
#include <iostream>
#include <iomanip>
#include <SDL.h>
#undef main

#include "fce_ines.hpp"
#include "fce_machine.hpp"

#include <cstdlib>

int main () try {
    auto famicom = libfce::machine ();
    famicom.load (libfce::ines::load ("dk.nes"));    
    SDL_Init (SDL_INIT_EVERYTHING);
    std::atexit (SDL_Quit);
    auto $window = SDL_CreateWindow ("Ohayoooo!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 341, 242, 0);
    auto $surface = SDL_GetWindowSurface ($window);
    auto $event = SDL_Event ();
    for (;;) {
        if (SDL_PollEvent (&$event)) {
            if ($event.type == SDL_QUIT) {
                break;
            }
            continue;
        }
        famicom.run_for_one_frame ();
        famicom.grab_frame ($surface->pixels);
    }
    SDL_DestroyWindow ($window);
    return 0;
    
}
catch (std::exception& e) {
    std::string what = e.what ();
    std::cout << e.what () << "\n";
}
