#ifndef __FCE_VIDEO_IMPL_HPP__
#define __FCE_VIDEO_IMPL_HPP__

#include "fce_ricoh_video.hpp"
#include "fce_utils.hpp"

namespace libfce {

    template <typename _MachineInterface, typename _BusInterface>
    inline ricoh_video<_MachineInterface, _BusInterface>::ricoh_video (
        _MachineInterface &mi, _BusInterface &sb)
    :   MachineInterface (mi),
        BusInterface (sb)
    {
        reset ();
    }

    template <typename _MachineInterface, typename _BusInterface>
    inline void ricoh_video<_MachineInterface, _BusInterface>::reset () {
        std::fill (std::begin (OamData), std::end (OamData), 0x0u);
    }

    template <typename _MachineInterface, typename _BusInterface>
    template <typename _SlaveBusInterface>
    inline bool ricoh_video<_MachineInterface, _BusInterface>
        ::cycle (_SlaveBusInterface& bus) 
    {     
        auto MasterClock =
            MachineInterface.clock ();
        tick (MasterClock*3u + 0);
        tick (MasterClock*3u + 1u);
        tick (MasterClock*3u + 2u);

        if (bus.address () >= 0x2000 && bus.address () < 0x4000) {            

            switch ((bus.address () - 0x2000) & 0x7) {
            }
          
            return true;
        }
        return false;        
    }

    template <typename _MachineInterface, typename _BusInterface>
    template <typename _ClockCounter>
    inline void ricoh_video<_MachineInterface, _BusInterface>
        ::tick (const _ClockCounter& clock) 
    {
        auto DotCycle = clock % _HorizontalDots;
        auto ScanLine = (clock / _HorizontalDots + 
            _VerticalLines - _VerticalBlank) %
            _VerticalLines - _PostRenderLines;
        if (ScanLine < _VerticalBlank) {
            //VerticalBlank = 1u;            
        }

    }

}
#endif