#ifndef __FCE_MACHINE_HPP__
#define __FCE_MACHINE_HPP__

#include "fce_ricoh_core.hpp"
#include "fce_ricoh_video.hpp"
#include "fce_memory.hpp"
#include "fce_bus_interface.hpp"
#include "fce_mapper.hpp"
#include "fce_ricoh_iologic.hpp"
#include <array>

namespace libfce {

    struct machine {

        typedef libfce::bus_interface<true> master_bus_type;
        typedef libfce::bus_interface<false> slave_bus_type;

        machine ();

        template <typename _CartData>
        void load (_CartData&& cart_);
        void run_for_one_frame ();        

        void hard_reset ();
        void soft_reset ();

        void grab_frame (void*);

        auto clock () const;

    protected:

        template <typename _Machine, typename _BusInterface>        
        friend struct ricoh_core;
        template <typename _Machine, typename _BusInterfece>        
        friend struct ricoh_video;
        template <typename _Machine, typename _BusInterface>
        friend struct ricoh_iologic;

        bool self_test () const;

        template <typename _BusInterface>
        bool unclocked_master_cycle (_BusInterface& bus);
        template <typename _BusInterface>
        bool master_cycle (_BusInterface& bus);
        template <typename _BusInterface>
        bool slave_cycle (_BusInterface& bus);

        void log_operation ();
        void log_state ();

    private:        
        std::uint64_t                                           MasterClock;    // CPU CLOCK
        master_bus_type                                         MasterBus;      // CPU BUS
        ricoh_core<machine, master_bus_type>                    RicohCore;      // CPU
        ricoh_iologic<machine, master_bus_type>                 RicohIoLogic;   // APU and I/O
        memory<master_bus_type, 0x0000, 0x2000, 0x800>          MasterRam;      // CPU RAM

        slave_bus_type                                          SlaveBus;       // PPU BUS
        ricoh_video<machine, slave_bus_type>                    RicohVideo;     // PPU        
        memory<slave_bus_type, 0x2000, 0x3F00, 0x1000>          SlaveRamMain;   // PPU Video Ram
        memory<slave_bus_type, 0x3F00, 0x4000, 0x20>            SlaveRamPal;    // PPU Palette Ram
        mapper<master_bus_type, slave_bus_type>                 Mapper;


    };
}

#include "fce_machine_impl.hpp"

#endif
