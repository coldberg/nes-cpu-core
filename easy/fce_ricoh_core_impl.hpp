#include "fce_ricoh_core.hpp"
#ifndef __FCE_ROICCOH_CORE_IMPL_HPP__
#define __FCE_ROICCOH_CORE_IMPL_HPP__

namespace libfce {
    template <typename _MachineInterface, typename _BusInterface>
    inline ricoh_core<_MachineInterface, _BusInterface>::ricoh_core (_MachineInterface& machine_, _BusInterface& bus_):
        State (machine_.self_test () ?
            0x0024000000FD0000ULL | _TestVector :
            0x0224000000FD0000ULL),
        MachineInterface (machine_),
        BusInterface (bus_)
    {}

    template<typename _MachineInterface, typename _BusInterface>
    inline std::uint8_t ricoh_core<_MachineInterface, _BusInterface>::read_cycle (std::uint16_t address_) {
        MachineInterface.master_cycle (
            BusInterface.init_read (address_));
        State.Nmi |= std::uint8_t (BusInterface.nmi ());
        State.Rst |= std::uint8_t (BusInterface.reset ());
        State.Irq |= std::uint8_t (BusInterface.irq ());
        return BusInterface.data ();
    }

    template<typename _MachineInterface, typename _BusInterface>
    inline std::uint8_t ricoh_core<_MachineInterface, _BusInterface>::write_cycle (std::uint16_t address_, std::uint8_t data_) {
        MachineInterface.master_cycle (!State.WriteDisabled ?
            BusInterface.init_write (address_, data_) :
            BusInterface.init_read (address_));
        State.Nmi |= std::uint8_t (BusInterface.nmi ());
        State.Rst |= std::uint8_t (BusInterface.reset ());
        State.Irq |= std::uint8_t (BusInterface.irq ());        
        return BusInterface.data ();        
    }

    template <typename _MachineInterface, typename _BusInterface>
    inline void ricoh_core<_MachineInterface, _BusInterface>::one () {
        typedef std::uint8_t    u8;
        typedef std::uint16_t   u16;
        typedef std::int8_t     i8;
        typedef std::int16_t    i16;

        enum interrupt_mode_t {
            im_brk = 0,
            im_nmi = 1,
            im_rst = 2,
            im_irq = 3
        };

        union word_t {
            struct { std::uint8_t l, h; };
            std::uint16_t w;
            word_t (): w (0) {}
            word_t (std::uint16_t w_): w (w_) {}
            word_t (std::uint8_t h_, std::uint8_t l_): l (l_), h (h_) {}
            word_t (const word_t& w_): w (w_.w) {}
        };

        static const std::uint16_t _Vector [] = {
            _IrqVector,
            _NmiVector,
            _ResetVector,
            _IrqVector
        };
                
        std::uint8_t o = 0u, t = 0u;

        if (u8 (State.Jammed)) {
            return;
        }

        word_t ad, ba;

        auto im = im_brk;

             if (State.Nmi) im = im_nmi, State.Nmi = 0;
        else if (State.Rst) im = im_rst, State.Rst = 0, State.WriteDisabled = 1;
        else if (State.Irq) im = im_irq, State.Irq = 0;
        
        ////////////////////////////////////////////////////////////////////////////
        /// Helper functions
        ////////////////////////////////////////////////////////////////////////////

        auto Op_Push = [&] (std::uint8_t x) {
            write_cycle (_StackVector | u8 (State.S), x); State.S--;
        };

        auto Op_StackRead = [&] {
            return read_cycle (_StackVector | u8 (State.S));
        };

        auto Op_Pop = [&] {
            ++State.S;
            return Op_StackRead ();
        };

        auto Op_FetchVector = [&] {
            State.Pcl = read_cycle (_Vector [im]);      
            State.Pch = read_cycle (_Vector [im]+1u);   
        };

        auto Op_UpdateNegAndZero = [&] (auto&& reg) {            
            auto o = u8 (reg);            
            State.N = (o >> 7)&1;
            State.Z = (o == 0);
        };

        auto Op_LoadImmediate = [&] (auto&& reg) {            
            reg = read_cycle (State.Pc++);      /*2*/
            Op_UpdateNegAndZero (reg);
        };

        auto Op_StoreZeroPage = [&] (auto&& reg) {
            ad.w = read_cycle (State.Pc++);   /*2*/
            write_cycle (ad.w, u8 (reg));          /*3*/
        };

        auto Op_Store = [&] (auto&& reg) {
            write_cycle (ad.w, reg);
        };

        auto Op_Branch = [&] (auto&& cond) {
            auto o = int (i8 (read_cycle (State.Pc++))); /*2*/
            if (cond) {                
                ad.w = u16 (State.Pc + o);
                read_cycle (State.Pc); /*3*/                
                if (ad.h == State.Pch) {
                    State.Pc = ad.w;
                    return;
                }                
                State.Pcl = ad.l;                
                read_cycle (State.Pc); /*4*/
                State.Pch = ad.h;                
            }      
        };

        auto Op_Increment = [&] (auto&& R) {
            read_cycle (State.Pc);                                  /*2*/
            R = u8 (u8 (R)+1);
            Op_UpdateNegAndZero (R);
        };

        auto Op_Decrement = [&] (auto&& R) {
            read_cycle (State.Pc);                                  /*2*/
            R = u8 (u8 (R)-1);
            Op_UpdateNegAndZero (R);
        };

        auto Op_Absolute = [&] {
            ad.l = read_cycle (State.Pc++);                         /*2*/
            ad.h = read_cycle (State.Pc++);                         /*3*/
        };

        auto Op_Immediate = [&] {
            ad.w = State.Pc++;
        };
             
        auto Op_ZeroPage = [&] {
            ad.w = read_cycle (State.Pc++);
        };

        auto Op_ZeroPageX = [&] {
            ad.w = read_cycle (State.Pc++);                         /*2*/
            read_cycle (ad.w);                                      /*3*/
            ad.l = (ad.l + u8 (State.X)) & 0xff;                    
        };

        auto Op_ZeroPageY = [&] {
            ad.w = read_cycle (State.Pc++);                         /*2*/
            read_cycle (ad.w);                                      /*3*/
            ad.l = (ad.l + u8 (State.Y)) & 0xff;
        };

        auto Op_AbsoluteIndexed_Impl = [&] (auto&& I, bool fc) {
            ad.l = read_cycle (State.Pc++);                         /*2*/
            ad.h = read_cycle (State.Pc++);                         /*3*/
            auto u = word_t (ad.w + u8 (I));
            if (u.h != ad.h || fc) {
                read_cycle (ad.w);                                  /*4*/                
            }
            ad.w = u.w;
        };

        auto Op_AbsoluteIndexed = [&] (auto&& I) {
            Op_AbsoluteIndexed_Impl (I, false);
        };

        auto Op_RMW_ZeroPage = [&] (auto&& op) {
            ad.w = read_cycle (State.Pc++);                         /*2*/
            t = read_cycle (ad.w);                                  /*3*/                        
            t = op (t);                                             
            write_cycle (ad.w, t);                                  /*4*/   // DUMMY WRITE TO ?
            write_cycle (ad.w, t);                                  /*5*/
            Op_UpdateNegAndZero (t);
        };

        auto Op_RMW_ZeroPageX = [&] (auto&& op) {
            ad.w = read_cycle (State.Pc++);                         /*2*/
            t = read_cycle (ad.w);                                  /*3*/
            ad.l = u8 (ad.l + u8 (State.X));
            t = read_cycle (ad.w);                                  /*4*/
            t = op (t);
            write_cycle (ad.w, t);                                  /*5*/   // DUMMY WRITE TO ?
            write_cycle (ad.w, t);                                  /*6*/
            Op_UpdateNegAndZero (t);
        };

        auto Op_RMW_Absolute = [&] (auto&& op) {
            ad.l = read_cycle (State.Pc++);                         /*2*/
            ad.h = read_cycle (State.Pc++);                         /*3*/
            t = read_cycle (ad.w);                                  /*4*/
            t = op (t);
            write_cycle (ad.w, t);                                  /*5*/   // DUMMY WRITE TO ?
            write_cycle (ad.w, t);                                  /*6*/
            Op_UpdateNegAndZero (t);
        };

        auto Op_RMW_AbsoluteX = [&] (auto&& op) {
            ad.l = read_cycle (State.Pc++);                         /*2*/
            ad.h = read_cycle (State.Pc++);                         /*3*/            
            auto at = word_t (ad.w + u8 (State.X));            
            ad.l = at.l; t = read_cycle (ad.w);                     /*4*/            
            ad.h = at.h; t = read_cycle (ad.w);                     /*5*/
            t = op (t);
            write_cycle (ad.w, t);                                  /*6*/   // DUMMY WRITE TO ?
            write_cycle (ad.w, t);                                  /*7*/
            Op_UpdateNegAndZero (t);
        };

        auto Op_RMW_Implied = [&] (auto&& op, auto&& R) {
            read_cycle (State.Pc);
            R = op (u8 (R));
            Op_UpdateNegAndZero (u8 (R));
        };

        auto Op_AbsoluteX = [&] {
            Op_AbsoluteIndexed (State.X);
        };

        auto Op_AbsoluteY = [&] {
            Op_AbsoluteIndexed (State.Y);
        };

        auto Op_IndirectX = [&] {
            ba.w = read_cycle (State.Pc++);         /*2*/
            read_cycle (ba.w);                      /*3*/ // dummy read, discard
            ba.l = u8 (ba.l + u8 (State.X));
            ad.l = read_cycle (ba.w);               /*4*/
            ba.l = u8 (ba.l + 1u);
            ad.h = read_cycle (ba.w);               /*5*/
        };

        auto Op_IndirectY_Impl = [&] (bool fc) {
            ad.w = read_cycle (State.Pc++);         /*2*/
            ba.l = read_cycle (ad.w);               /*3*/
            ad.l = u8 (ad.l + 1u);
            ba.h = read_cycle (ad.w);               /*4*/
            ad.w = ba.w + u8 (State.Y);
            if (ad.h != ba.h || fc) {
                ba.l = u8 (ba.l + u8 (State.Y));
                read_cycle (ba.w);                  /*5*/ // dummy read
            }
        };

        auto Op_IndirectY = [&] {
            Op_IndirectY_Impl (false);
        };

        auto Op_StoreZeroPageIndexed = [&] (auto&& R, auto&& I) {
            ad.w = read_cycle (State.Pc++);     /*2*/
            t = read_cycle (ad.w);              /*3*/
            ad.l = u8 (ad.l + u8 (I));
            write_cycle (ad.w, u8 (R));         /*4*/
        };

        auto Op_StoreAbsolute = [&] (auto&& R) {
            Op_Absolute ();                     /*2-3*/
            write_cycle (ad.w, u8 (R));         /*4*/
        };

        auto Op_StoreAbsoluteIndexed = [&] (auto&& R, auto&& I) {
            Op_AbsoluteIndexed_Impl (I, true);  /* 2-4 */
            write_cycle (ad.w, u8 (R));         /* 5 */
        };

        auto Op_StoreIndirectX = [&] (auto&& R) {
            Op_IndirectX ();
            write_cycle (ad.w, u8 (R));
        };

        auto Op_StoreIndirectY = [&] (auto&& R) {
            Op_IndirectY_Impl (true);           /*2-5*/
            write_cycle (ad.w, u8 (R));         /*6*/
        };

        auto Op_LoadZeroPage = [&] (auto&& R) {
            ad.w = read_cycle (State.Pc++);     /*2*/
            R = read_cycle (ad.w);              /*3*/
            Op_UpdateNegAndZero (u8 (R));
        };

        auto Op_LoadZeroPageIndexed = [&] (auto&& R, auto&& I) {
            ad.w = read_cycle (State.Pc++);     /*2*/   // fetch BAL
            read_cycle (ad.w);                  /*3*/   // fetch discard 
            ad.l = u8 (ad.l + u8 (I));                 
            R = read_cycle (ad.w);              /*4*/   // fetch data
            Op_UpdateNegAndZero (u8 (R));
        };

        auto Op_LoadAbsolute = [&] (auto&& R) {
            Op_Absolute ();                     /*2-3*/
            R = read_cycle (ad.w);              /*4*/
            Op_UpdateNegAndZero (u8 (R));
        };

        auto Op_LoadAbsoluteIndexed = [&] (auto&& R, auto&& I) {
            Op_AbsoluteIndexed (I);             /* 2-3/4 */
            R = read_cycle (ad.w);              /* 4/5 */
            Op_UpdateNegAndZero (u8 (R));
        };

        auto Op_LoadIndirectX = [&] (auto&& R) {
            Op_IndirectX ();                    /*2-5*/
            R = read_cycle (ad.w);              /*6*/
            Op_UpdateNegAndZero (u8 (R));
        };

        auto Op_LoadIndirectY = [&] (auto&& R) {
            Op_IndirectY ();                    /*2-4/5*/
            R = read_cycle (ad.w);              /*6/5*/
            Op_UpdateNegAndZero (u8 (R));
        };

        auto Op_LoadAX = [&] {
            auto tt = read_cycle (ad.w);
            State.X = tt;
            State.A = tt;
            Op_UpdateNegAndZero (tt);
        };

        auto Op_StoreXAndA = [&] {
            auto tt = u8 (State.X) & u8 (State.A);
            write_cycle (ad.w, tt);
            //Op_UpdateNegAndZero (tt);
        };

        auto Op_CmpTo = [&] (auto&& R, auto&& M) {
            auto t = u16 (R) + ~u16 (M) + 1;
            State.C = (~t >> 8)&1;
            Op_UpdateNegAndZero (t&0xff);
        };

        auto Op_Cmp = [&] (auto&& M) {
            Op_CmpTo (State.A, M);
        };

        auto Op_CpX = [&] (auto&& M) {
            Op_CmpTo (State.X, M);
        };

        auto Op_CpY = [&] (auto&& M) {
            Op_CmpTo (State.Y, M);
        };

        auto Op_Dcp = [&] {            
            auto tt = read_cycle (ad.w) - 1u;
            write_cycle (ad.w, tt);
            write_cycle (ad.w, tt);
            tt = u16 (State.A) + u16 ((~tt)&0xff) + 1;
            State.C = (tt >> 8)&1;
            Op_UpdateNegAndZero (tt & 0xff);
        };

        auto Op_UpdateOverflow = [&] (auto&& N, auto&& M, auto&& O) {
            State.V = !!((u16 (N)^u16 (M))&(u16 (N)^u16 (O))&0x80);
        };

        auto Op_Bit = [&] (auto&& M) {
            State.N =  !!(M & 0x80);
            State.V =  !!(M & 0x40);
            State.Z = !(M & u8 (State.A));
        };

        auto Op_And = [&] (auto&& M) {
            Op_UpdateNegAndZero (State.A &= u8 (M));
        };

        auto Op_Ora = [&] (auto&& M) {
            Op_UpdateNegAndZero (State.A |= u8 (M));
        };

        auto Op_Eor = [&] (auto&& M) {
            Op_UpdateNegAndZero (State.A ^= u8 (M));
        };

        auto Op_Lsr = [&] (auto&& R) {
            auto pc = u16 (State.Pc);            
            State.C = u8 (R) & 1;            
            return u8 (R) >> 1;
        };
        auto Op_Asl = [&] (auto&& R) {
            State.C = u8 (R) >> 7;            
            return u8 (R) << 1;
        };

        auto Op_Rol = [&] (auto&& R) {
            auto _Ci = u8 (State.C);            
            State.C = (u8 (R) >> 7)&1;
            return (u8 (R) << 1) | _Ci;
        };

        auto Op_Ror = [&] (auto&& R) {            
            auto _Ci = u8 (State.C);
            State.C = u8 (R)&1;            
            return (u8 (R) >> 1) | (_Ci << 7);
        };

        auto Op_Adc = [&] (auto&& M) {
            auto t = u16 (State.A) + u16 (M) + u16 (State.C);          
            State.C = (t >> 8)&1;
            Op_UpdateOverflow (t, State.A, M);
            Op_UpdateNegAndZero (u8 (t));
            State.A = u8 (t);
        };

        auto Op_Sbc = [&] (auto&& M) {
            Op_Adc (u8 (M) ^ 0xFF);
        };


        auto Op_Isb = [&] {
            auto tt = (read_cycle (ad.w) + 1u)&0xff;
            write_cycle (ad.w, tt);
            write_cycle (ad.w, tt);
            tt ^= 0xFF;
            auto rr = u16 (State.A) + u16 (tt) + u16 (State.C);
            Op_UpdateOverflow (rr, State.A, tt);
            Op_UpdateNegAndZero (u8 (rr));
            State.A = u8 (rr);
        };

        auto Op_Slo = [&] {
            auto tt = (u16 (read_cycle (ad.w)) << 1u);
            write_cycle (ad.w, tt);
            write_cycle (ad.w, tt);
            State.C = (tt >> 8)&1;         
            State.A = u8 (u8 (State.A) | tt);
            Op_UpdateNegAndZero (u8 (State.A));
        };

        auto Op_Sre = [&] {
            auto tt = u16 (read_cycle (ad.w));
            State.C = tt & 1;
            tt = tt >> 1u;
            write_cycle (ad.w, u8 (tt));
            write_cycle (ad.w, u8 (tt));
            State.A = u8 (u8 (State.A) ^ tt);
            Op_UpdateNegAndZero (u8 (State.A));
        };

        auto Op_Rla = [&] {
            auto tt = u16 (read_cycle (ad.w)) << 1u;
            auto Co = (tt >> 8) & 1;
            tt = u16 (State.C)|tt;
            State.C = Co;
            write_cycle (ad.w, u8 (tt));
            write_cycle (ad.w, u8 (tt));
            State.A = u8 (u8 (State.A) & tt);
            Op_UpdateNegAndZero (u8 (State.A));
        };

        auto Op_Rra = [&] {
            auto tt = u16 (read_cycle (ad.w));
            auto Co = tt & 1u;
            tt = u8 ((u16 (State.C) << 7)|(tt >> 1u));            
            write_cycle (ad.w, u8 (tt));
            write_cycle (ad.w, u8 (tt));
            auto rr = u16 (State.A) + u16 (tt) + Co;
            State.C = (rr >> 8)&1;
            Op_UpdateOverflow (rr, u8 (State.A), tt);
            Op_UpdateNegAndZero (u8 (rr));
            State.A = u8 (rr);
        };


        auto Op_TransferN = [&] (auto&& S, auto&& D) {
            read_cycle (State.Pc);
            D = u8 (S);
        };

        auto Op_TransferF = [&] (auto&& S, auto&& D) {
            read_cycle (State.Pc);
            D = u8 (S);
            Op_UpdateNegAndZero (u8 (D));
        };

        auto Op_SubOne = [] (auto&& x) { 
            return u8 (x-1u); 
        };

        auto Op_AddOne = [] (auto&& x) { 
            return u8 (x+1u); 
        };

        ////////////////////////////////////////////////////////////////////////////
        /// Main switch block
        ////////////////////////////////////////////////////////////////////////////

        switch (o = im ? 0x00 : read_cycle (State.Pc++)) {

            ////////////////////////////////////////////////////////////////
            /// NOP
            ////////////////////////////////////////////////////////////////
            case 0xEA: read_cycle (State.Pc); break;                // NOP Implied
            case 0x80: Op_Immediate (); read_cycle (ad.w); break;   //*DNOP Immediate    
            case 0x82: Op_Immediate (); read_cycle (ad.w); break;   //*DNOP Immediate    
            case 0x89: Op_Immediate (); read_cycle (ad.w); break;   //*DNOP Immediate    
            case 0xC2: Op_Immediate (); read_cycle (ad.w); break;   //*DNOP Immediate    
            case 0xE2: Op_Immediate (); read_cycle (ad.w); break;   //*DNOP Immediate    
            case 0x04: Op_ZeroPage  (); read_cycle (ad.w); break;   //*DNOP Zero Page    
            case 0x64: Op_ZeroPage  (); read_cycle (ad.w); break;   //*DNOP Zero Page    
            case 0x44: Op_ZeroPage  (); read_cycle (ad.w); break;   //*DNOP Zero Page    
            case 0x14: Op_ZeroPageX (); read_cycle (ad.w); break;   //*DNOP Zero Page, X 
            case 0x34: Op_ZeroPageX (); read_cycle (ad.w); break;   //*DNOP Zero Page, X 
            case 0x54: Op_ZeroPageX (); read_cycle (ad.w); break;   //*DNOP Zero Page, X 
            case 0x74: Op_ZeroPageX (); read_cycle (ad.w); break;   //*DNOP Zero Page, X 
            case 0xD4: Op_ZeroPageX (); read_cycle (ad.w); break;   //*DNOP Zero Page, X 
            case 0xF4: Op_ZeroPageX (); read_cycle (ad.w); break;   //*DNOP Zero Page, X 
            case 0x0C: Op_Absolute  (); read_cycle (ad.w); break;   //*TNOP Absolute
            case 0x1C: Op_AbsoluteX (); read_cycle (ad.w); break;   //*TNOP Absolute, X
            case 0x3C: Op_AbsoluteX (); read_cycle (ad.w); break;   //*TNOP Absolute, X
            case 0x5C: Op_AbsoluteX (); read_cycle (ad.w); break;   //*TNOP Absolute, X
            case 0x7C: Op_AbsoluteX (); read_cycle (ad.w); break;   //*TNOP Absolute, X
            case 0xDC: Op_AbsoluteX (); read_cycle (ad.w); break;   //*TNOP Absolute, X
            case 0xFC: Op_AbsoluteX (); read_cycle (ad.w); break;   //*TNOP Absolute, X
            case 0x1A: read_cycle (State.Pc); break;                //*NOP Implied
            case 0x3A: read_cycle (State.Pc); break;                //*NOP Implied
            case 0x5A: read_cycle (State.Pc); break;                //*NOP Implied
            case 0x7A: read_cycle (State.Pc); break;                //*NOP Implied
            case 0xDA: read_cycle (State.Pc); break;                //*NOP Implied
            case 0xFA: read_cycle (State.Pc); break;                //*NOP Implied            
                    
            ///////////////////////////////////////////////////////////////
            /// Control flow
            ///////////////////////////////////////////////////////////////
            case 0x00:  // Brk/Nmi/Irq/Reset
                read_cycle (State.Pc);                                      /*2*/
                Op_Push (State.Pch);                                        /*3*/
                Op_Push (State.Pcl);                                        /*4*/
                Op_Push (u8(State.P) | (im ? 0x20 : 0x30));                 /*5*/                       
                Op_FetchVector ();                                          /*6-7*/                
                break;
            case 0x4C:  // JMP Absolute             
                ad.l = read_cycle (State.Pc++);                             /*2*/
                ad.h = read_cycle (State.Pc++);                             /*3*/
                State.Pc = ad.w;
                break;
            case 0x6C:
                ba.l = read_cycle (State.Pc++);                             /*2*/
                ba.h = read_cycle (State.Pc++);                             /*3*/
                State.Pcl = read_cycle (ba.w);                              /*4*/
                ba.l = u8 (ba.l + 1u);                                      
                State.Pch = read_cycle (ba.w);                              /*5*/
                break;
            case 0x20:  // JSR Abs
                ad.l = read_cycle (State.Pc++);                             /*2*/
                write_cycle (_StackVector | u8 (State.S), ad.l);            /*3*/
                Op_Push (State.Pch);                                        /*4*/
                Op_Push (State.Pcl);                                        /*5*/
                ad.h = read_cycle (State.Pc);                               /*6*/
                State.Pc = ad.w;
                break;
            case 0x40:  // RTI
                read_cycle (State.Pc);                                      /*2*/
                Op_StackRead ();                                            /*3*/
                State.P = Op_Pop ();                                        /*4*/
                State.Pcl = Op_Pop ();                                      /*5*/  
                State.Pch = Op_Pop ();                                      /*6*/
                State._One = 1;
                break;
            case 0x60:  // RTS
                read_cycle (State.Pc);                                      /*2*/
                Op_StackRead ();                                            /*3*/
                State.Pcl = Op_Pop ();                                      /*4*/
                State.Pch = Op_Pop ();                                      /*5*/
                read_cycle (State.Pc); ++State.Pc;                          /*6*/
                break;

            ///////////////////////////////////////////////////////////////
            /// Set flag
            ///////////////////////////////////////////////////////////////
            case 0x38: read_cycle (State.Pc); State.C = 1; break;           // SEC Implied
            case 0xF8: read_cycle (State.Pc); State.D = 1; break;           // SED Implied
            case 0x78: read_cycle (State.Pc); State.I = 1; break;           // SEI Implied

            ///////////////////////////////////////////////////////////////
            /// Clear flag
            ///////////////////////////////////////////////////////////////
            case 0x18: read_cycle (State.Pc); State.C = 0; break;           // CLC Implied
            case 0xD8: read_cycle (State.Pc); State.D = 0; break;           // CLD Implied
            case 0x58: read_cycle (State.Pc); State.I = 0; break;           // CLI Implied
            case 0xB8: read_cycle (State.Pc); State.V = 0; break;           // CLV Implied

            ///////////////////////////////////////////////////////////////
            /// Branches
            ///////////////////////////////////////////////////////////////
            case 0x10: Op_Branch (!State.N); break;                         // BPL Implied
            case 0x30: Op_Branch ( State.N); break;                         // BMI Implied
            case 0x50: Op_Branch (!State.V); break;                         // BVC Implied
            case 0x70: Op_Branch ( State.V); break;                         // BVS Implied
            case 0x90: Op_Branch (!State.C); break;                         // BCC Implied
            case 0xB0: Op_Branch ( State.C); break;                         // BCS Implied
            case 0xD0: Op_Branch (!State.Z); break;                         // BNE Implied
            case 0xF0: Op_Branch ( State.Z); break;                         // BEQ Implied

            ///////////////////////////////////////////////////////////////
            /// Increment
            ///////////////////////////////////////////////////////////////
            case 0xE8: Op_Increment (State.X); break;                       // INX Implied
            case 0xC8: Op_Increment (State.Y); break;                       // INY Implied

            ///////////////////////////////////////////////////////////////
            /// Decrement
            ///////////////////////////////////////////////////////////////
            case 0xCA: Op_Decrement (State.X); break;                       // DEX Implied
            case 0x88: Op_Decrement (State.Y); break;                       // DEY Implied

            ///////////////////////////////////////////////////////////////
            /// TRANSFER
            ///////////////////////////////////////////////////////////////
            case 0x8A: Op_TransferF (State.X, State.A); break;              // TXA Implied
            case 0x98: Op_TransferF (State.Y, State.A); break;              // TYA Implied
            case 0xA8: Op_TransferF (State.A, State.Y); break;              // TAY Implied
            case 0xAA: Op_TransferF (State.A, State.X); break;              // TAX Implied
            case 0x9A: Op_TransferN (State.X, State.S); break;              // TXS Implied
            case 0xBA: Op_TransferF (State.S, State.X); break;              // TSX Implied

            ///////////////////////////////////////////////////////////////
            /// STY
            ///////////////////////////////////////////////////////////////
            case 0x84: Op_StoreZeroPage (State.Y); break;                   // STY ZeroPage
            case 0x94: Op_StoreZeroPageIndexed (State.Y, State.X); break;   // STY ZeroPage,X
            case 0x8C: Op_StoreAbsolute (State.Y); break;                   // STY Absolute

            ///////////////////////////////////////////////////////////////
            /// STX
            ///////////////////////////////////////////////////////////////
            case 0x86: Op_StoreZeroPage (State.X); break;                   // STX ZeroPage
            case 0x96: Op_StoreZeroPageIndexed (State.X, State.Y); break;   // STX ZeroPage,Y
            case 0x8E: Op_StoreAbsolute (State.X); break;                   // STX Absolute

            ///////////////////////////////////////////////////////////////
            /// STA
            ///////////////////////////////////////////////////////////////
            case 0x85: Op_StoreZeroPage (State.A); break;                   // STA ZeroPage
            case 0x95: Op_StoreZeroPageIndexed (State.A, State.X); break;   // STA ZeroPage,X
            case 0x8D: Op_StoreAbsolute (State.A); break;                   // STA Absolute
            case 0x99: Op_StoreAbsoluteIndexed (State.A, State.Y); break;   // STA Absolute,X
            case 0x9D: Op_StoreAbsoluteIndexed (State.A, State.X); break;   // STA Absolute,Y
            case 0x81: Op_StoreIndirectX (State.A); break;                  // STA Indirect,X
            case 0x91: Op_StoreIndirectY (State.A); break;                  // STA Indirect,Y    

            ///////////////////////////////////////////////////////////////
            /// SAX
            ///////////////////////////////////////////////////////////////
            case 0x87: Op_ZeroPage  (); Op_StoreXAndA (); break;            // SAX ZeroPage
            case 0x97: Op_ZeroPageY (); Op_StoreXAndA (); break;            // SAX ZeroPage, Y
            case 0x83: Op_IndirectX (); Op_StoreXAndA (); break;            // SAX Indirect, X
            case 0x8F: Op_Absolute  (); Op_StoreXAndA (); break;            // SAX Absolute

            ///////////////////////////////////////////////////////////////
            /// LDX
            ///////////////////////////////////////////////////////////////
            case 0xA2: Op_LoadImmediate (State.X); break;                   // LDX Immediate
            case 0xA6: Op_LoadZeroPage (State.X); break;                    // LDX ZeroPage
            case 0xB6: Op_LoadZeroPageIndexed (State.X, State.Y); break;    // LDX ZeroPage,Y
            case 0xAE: Op_LoadAbsolute (State.X); break;                    // LDX Absolute
            case 0xBE: Op_LoadAbsoluteIndexed (State.X, State.Y); break;    // LDX Absolute,Y

            ///////////////////////////////////////////////////////////////
            /// LDY
            ///////////////////////////////////////////////////////////////
            case 0xA0: Op_LoadImmediate (State.Y); break;                   // LDY Immediate                        
            case 0xA4: Op_LoadZeroPage (State.Y); break;                    // LDY ZeroPage
            case 0xB4: Op_LoadZeroPageIndexed (State.Y, State.X); break;    // LDY ZeroPage,X
            case 0xAC: Op_LoadAbsolute (State.Y); break;                    // LDY Absolute
            case 0xBC: Op_LoadAbsoluteIndexed (State.Y, State.X); break;    // LDY Absolute,X

            ///////////////////////////////////////////////////////////////
            /// LDA
            ///////////////////////////////////////////////////////////////
            case 0xA9: Op_LoadImmediate (State.A); break;                   // LDA Immediate
            case 0xA5: Op_LoadZeroPage (State.A); break;                    // LDA ZeroPage
            case 0xB5: Op_LoadZeroPageIndexed (State.A, State.X); break;    // LDA ZeroPage,X
            case 0xAD: Op_LoadAbsolute (State.A); break;                    // LDA Absolute
            case 0xBD: Op_LoadAbsoluteIndexed (State.A, State.X); break;    // LDA Absolute,X
            case 0xB9: Op_LoadAbsoluteIndexed (State.A, State.Y); break;    // LDA Absolute,Y
            case 0xA1: Op_LoadIndirectX (State.A); break;                   // LDA (Indirect, X)
            case 0xB1: Op_LoadIndirectY (State.A); break;                   // LDA (Indirect), Y

            ///////////////////////////////////////////////////////////////
            /// LAX
            ///////////////////////////////////////////////////////////////
            case 0xA7: Op_ZeroPage  (); Op_LoadAX (); break;                // LAX ZreoPage
            case 0xB7: Op_ZeroPageY (); Op_LoadAX (); break;                // LAX ZeroPage,Y
            case 0xAF: Op_Absolute  (); Op_LoadAX (); break;                // LAX Absolute
            case 0xBF: Op_AbsoluteY (); Op_LoadAX (); break;                // LAX Absolute,Y
            case 0xA3: Op_IndirectX (); Op_LoadAX (); break;                // LAX (Indirect,X)
            case 0xB3: Op_IndirectY (); Op_LoadAX (); break;                // LAX (Indirect),Y


            ///////////////////////////////////////////////////////////////
            /// LSR
            ///////////////////////////////////////////////////////////////
            case 0x4A: Op_RMW_Implied   (Op_Lsr, State.A); break;           // LSR A Implied
            case 0x46: Op_RMW_ZeroPage  (Op_Lsr); break;                    // LSR ZeroPage
            case 0x56: Op_RMW_ZeroPageX (Op_Lsr); break;                    // LSR ZeroPage,X
            case 0x4E: Op_RMW_Absolute  (Op_Lsr); break;                    // LSR Absolute
            case 0x5E: Op_RMW_AbsoluteX (Op_Lsr); break;                    // LSR Absolute,X

            ///////////////////////////////////////////////////////////////
            /// ASL
            ///////////////////////////////////////////////////////////////
            case 0x0A: Op_RMW_Implied   (Op_Asl, State.A); break;           // ASL A Implied     
            case 0x06: Op_RMW_ZeroPage  (Op_Asl); break;                    // ASL ZeroPage
            case 0x16: Op_RMW_ZeroPageX (Op_Asl); break;                    // ASL ZeroPage,X
            case 0x0E: Op_RMW_Absolute  (Op_Asl); break;                    // ASL Absolute
            case 0x1E: Op_RMW_AbsoluteX (Op_Asl); break;                    // ASL Absolute,X

            ///////////////////////////////////////////////////////////////
            /// ROR
            ///////////////////////////////////////////////////////////////
            case 0x6A: Op_RMW_Implied   (Op_Ror, State.A); break;           // ROR A Implied     
            case 0x66: Op_RMW_ZeroPage  (Op_Ror); break;                    // ROR ZeroPage
            case 0x76: Op_RMW_ZeroPageX (Op_Ror); break;                    // ROR ZeroPage,X
            case 0x6E: Op_RMW_Absolute  (Op_Ror); break;                    // ROR Absolute
            case 0x7E: Op_RMW_AbsoluteX (Op_Ror); break;                    // ROR Absolute,X

            ///////////////////////////////////////////////////////////////
            /// ROL
            ///////////////////////////////////////////////////////////////
            case 0x2A: Op_RMW_Implied   (Op_Rol, State.A); break;           // ROL A Implied     
            case 0x26: Op_RMW_ZeroPage  (Op_Rol); break;                    // ROL ZeroPage
            case 0x36: Op_RMW_ZeroPageX (Op_Rol); break;                    // ROL ZeroPage,X
            case 0x2E: Op_RMW_Absolute  (Op_Rol); break;                    // ROL Absolute
            case 0x3E: Op_RMW_AbsoluteX (Op_Rol); break;                    // ROL Absolute,X                    

            ///////////////////////////////////////////////////////////////
            /// INC
            ///////////////////////////////////////////////////////////////
            case 0xE6: Op_RMW_ZeroPage  (Op_AddOne); break;                    // INC ZeroPage
            case 0xF6: Op_RMW_ZeroPageX (Op_AddOne); break;                    // INC ZeroPage,X
            case 0xEE: Op_RMW_Absolute  (Op_AddOne); break;                    // INC Absolute
            case 0xFE: Op_RMW_AbsoluteX (Op_AddOne); break;                    // INC Absolute,X

            ///////////////////////////////////////////////////////////////
            /// DEC
            ///////////////////////////////////////////////////////////////
            case 0xC6: Op_RMW_ZeroPage  (Op_SubOne); break;                     // DEC ZeroPage
            case 0xD6: Op_RMW_ZeroPageX (Op_SubOne); break;                     // DEC ZeroPage,X
            case 0xCE: Op_RMW_Absolute  (Op_SubOne); break;                     // DEC Absolute
            case 0xDE: Op_RMW_AbsoluteX (Op_SubOne); break;                     // DEC Absolute,X

            ///////////////////////////////////////////////////////////////
            /// DCP
            ///////////////////////////////////////////////////////////////
            case 0xC7: Op_ZeroPage  (); Op_Dcp (); break;
            case 0xD7: Op_ZeroPageX (); Op_Dcp (); break;
            case 0xCF: Op_Absolute  (); Op_Dcp (); break;
            case 0xDF: Op_AbsoluteX (); Op_Dcp (); break;
            case 0xDB: Op_AbsoluteY (); Op_Dcp (); break;
            case 0xC3: Op_IndirectX (); Op_Dcp (); break;
            case 0xD3: Op_IndirectY (); Op_Dcp (); break;

            //////////////////////////////////////////////////////////
            /// PUSH/PULL
            //////////////////////////////////////////////////////////
            case 0x48: // PHA
                read_cycle (State.Pc);                              /*2*/
                Op_Push (State.A);                                  /*3*/
                break;
            case 0x08: // PHP
                read_cycle (State.Pc);                              /*2*/
                Op_Push (u8 (State.P) | 0x10);                      /*3*/
                break;
            case 0x68: // PLA
                read_cycle (State.Pc);                              /*2*/
                Op_StackRead ();                                    /*3*/
                State.A = Op_Pop ();                                /*4*/
                Op_UpdateNegAndZero (State.A);
                break;
            case 0x28: // PLP
                read_cycle (State.Pc);                              /*2*/
                Op_StackRead ();                                    /*3*/
                State.P = Op_Pop ();                                /*4*/
                State._One = 1;
                State.B = 0;
                break;

            //////////////////////////////////////////////////////////
            /// BIT
            //////////////////////////////////////////////////////////
            case 0x24: // BIT ZeroPage
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_Bit (read_cycle (ad.w));                         /*3*/
                break;
            case 0x2C: // BIT Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_Bit (read_cycle (ad.w));                         /*4*/
                break;

            //////////////////////////////////////////////////////////
            /// AND
            //////////////////////////////////////////////////////////
            case 0x29:  // AND Immediate                                                         
                Op_And (read_cycle (State.Pc++));                   /*2*/
                break;
            case 0x25:  // AND ZeroPage
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_And (read_cycle (ad.w));                         /*3*/
                break;
            case 0x35:  // AND ZeroPage, X
                Op_ZeroPageX ();                                    /*2-3*/
                Op_And (read_cycle (ad.w));                         /*4*/
                break;
            case 0x2D:  // AND Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_And (read_cycle (ad.w));                         /*4*/
                break;
            case 0x3D:  // AND Absolute, X
                Op_AbsoluteX ();                                    /*2-4*/
                Op_And (read_cycle (ad.w));                         /*5*/
                break;
            case 0x39:  // AND Absolute, Y
                Op_AbsoluteY ();                                    /*2-4*/
                Op_And (read_cycle (ad.w));                         /*5*/
                break;
            case 0x21:  // AND (Indirect, X)
                Op_IndirectX ();
                Op_And (read_cycle (ad.w));
                break;
            case 0x31:  // AND (Indirect), Y
                Op_IndirectY ();
                Op_And (read_cycle (ad.w));
                break;

            //////////////////////////////////////////////////////////
            /// ORA
            //////////////////////////////////////////////////////////
            case 0x09:  // ORA Immediate                                                         
                Op_Ora (read_cycle (State.Pc++));                   /*2*/
                break;
            case 0x05:  // ORA ZeroPage
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_Ora (read_cycle (ad.w));                         /*3*/
                break;
            case 0x15:  // ORA ZeroPage, X
                Op_ZeroPageX ();                                    /*2-3*/
                Op_Ora (read_cycle (ad.w));                         /*4*/
                break;
            case 0x0D:  // ORA Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_Ora (read_cycle (ad.w));                         /*4*/
                break;
            case 0x1D:  // ORA Absolute, X
                Op_AbsoluteX ();                                    /*2-4*/
                Op_Ora (read_cycle (ad.w));                         /*5*/
                break;
            case 0x19:  // ORA Absolute, Y
                Op_AbsoluteY ();                                    /*2-4*/
                Op_Ora (read_cycle (ad.w));                         /*5*/
                break;
            case 0x01:  // ORA (Indirect, X)
                Op_IndirectX ();
                Op_Ora (read_cycle (ad.w));
                break;
            case 0x11:  // ORA (Indirect), Y
                Op_IndirectY ();
                Op_Ora (read_cycle (ad.w));
                break;

            //////////////////////////////////////////////////////////
            /// EOR
            //////////////////////////////////////////////////////////
            case 0x49:  // EOR Immediate                                                         
                Op_Eor (read_cycle (State.Pc++));                   /*2*/
                break;
            case 0x45:  // EOR ZeroPage
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_Eor (read_cycle (ad.w));                         /*3*/
                break;
            case 0x55:  // EOR ZeroPage, X
                Op_ZeroPageX ();                                    /*2-3*/
                Op_Eor (read_cycle (ad.w));                         /*4*/
                break;
            case 0x4D:  // EOR Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_Eor (read_cycle (ad.w));                         /*4*/
                break;
            case 0x5D:  // EOR Absolute, X
                Op_AbsoluteX ();                                    /*2-4*/
                Op_Eor (read_cycle (ad.w));                         /*5*/
                break;
            case 0x59:  // EOR Absolute, Y
                Op_AbsoluteY ();                                    /*2-4*/
                Op_Eor (read_cycle (ad.w));                         /*5*/
                break;
            case 0x41:  // EOR (Indirect, X)
                Op_IndirectX ();
                Op_Eor (read_cycle (ad.w));
                break;
            case 0x51:  // EOR (Indirect), Y
                Op_IndirectY ();
                Op_Eor (read_cycle (ad.w));
                break;

            //////////////////////////////////////////////////////////
            /// CMP
            //////////////////////////////////////////////////////////
            case 0xC9:  // CMP Immediate
                Op_Cmp (read_cycle (State.Pc++));                   /*2*/
                break;
            case 0xC5:  // CMP Zero Page
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_Cmp (read_cycle (ad.w));                         /*3*/
                break;
            case 0xD5:  // CMP Zero Page, X
                Op_ZeroPageX ();                                    /*2-3*/
                Op_Cmp (read_cycle (ad.w));                         /*4*/
                break;
            case 0xCD:  // CMP Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_Cmp (read_cycle (ad.w));                         /*4*/
                break;
            case 0xDD:  // CMP Absolute, X
                Op_AbsoluteX ();                                    /*2-4*/
                Op_Cmp (read_cycle (ad.w));                         /*5*/
                break;
            case 0xD9:  // CMP Absolute, Y
                Op_AbsoluteY ();                                    /*2-4*/
                Op_Cmp (read_cycle (ad.w));                         /*5*/
                break;
            case 0xC1:  // CMP (Indirect, X)
                Op_IndirectX ();       
                Op_Cmp (read_cycle (ad.w));
                break;
            case 0xD1:  // CMP (Indirect), Y
                Op_IndirectY ();
                Op_Cmp (read_cycle (ad.w));
                break;

            //////////////////////////////////////////////////////////
            /// CPX
            //////////////////////////////////////////////////////////
            case 0xE0:  // CPX Immediate
                Op_CpX (read_cycle (State.Pc++));                   /*2*/
                break;
            case 0xE4:  // CPX ZeroPage                
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_CpX (read_cycle (ad.w));                         /*3*/
                break;
            case 0xEC:  // CPX Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_CpX (read_cycle (ad.w));                         /*4*/
                break;

            //////////////////////////////////////////////////////////
            /// CPY
            //////////////////////////////////////////////////////////
            case 0xC0:  // CPY Immediate                
                Op_CpY (read_cycle (State.Pc++));
                break;
            case 0xC4:  // CPY ZeroPage                
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_CpY (read_cycle (ad.w));                         /*3*/
                break;
            case 0xCC:  // CPY Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_CpY (read_cycle (ad.w));                         /*4*/
                break;

            //////////////////////////////////////////////////////////
            /// ADC
            //////////////////////////////////////////////////////////
            case 0x69:  // ADC Immediate
                Op_Adc (read_cycle (State.Pc++));                   /*2*/
                break;
            case 0x65:  // ADC Zero Page
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_Adc (read_cycle (ad.w));                         /*3*/
                break;
            case 0x75:  // ADC Zero Page, X
                Op_ZeroPageX ();                                    /*2-3*/
                Op_Adc (read_cycle (ad.w));                         /*4*/
                break;
            case 0x6D:  // ADC Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_Adc (read_cycle (ad.w));                         /*4*/
                break;
            case 0x7D:  // ADC Absolute, X
                Op_AbsoluteX ();                                    /*2-4*/
                Op_Adc (read_cycle (ad.w));                         /*5*/
                break;
            case 0x79:  // ADC Absolute, Y
                Op_AbsoluteY ();                                    /*2-4*/
                Op_Adc (read_cycle (ad.w));                         /*5*/
                break;
            case 0x61:  // ADC (Indirect, X)
                Op_IndirectX ();
                Op_Adc (read_cycle (ad.w));
                break;
            case 0x71:  // ADC (Indirect), Y
                Op_IndirectY ();
                Op_Adc (read_cycle (ad.w));
                break;

            //////////////////////////////////////////////////////////
            /// SBC
            //////////////////////////////////////////////////////////
            case 0xEB:
            case 0xE9:  // SBC Immediate
                Op_Sbc (read_cycle (State.Pc++));                   /*2*/
                break;
            case 0xE5:  // SBC Zero Page
                ad.w = read_cycle (State.Pc++);                     /*2*/
                Op_Sbc (read_cycle (ad.w));                         /*3*/
                break;
            case 0xF5:  // SBC Zero Page, X
                Op_ZeroPageX ();                                    /*2-3*/
                Op_Sbc (read_cycle (ad.w));                         /*4*/
                break;
            case 0xED:  // SBC Absolute
                Op_Absolute ();                                     /*2-3*/
                Op_Sbc (read_cycle (ad.w));                         /*4*/
                break;
            case 0xFD:  // SBC Absolute, X
                Op_AbsoluteX ();                                    /*2-4*/
                Op_Sbc (read_cycle (ad.w));                         /*5*/
                break;
            case 0xF9:  // SBC Absolute, Y
                Op_AbsoluteY ();                                    /*2-4*/
                Op_Sbc (read_cycle (ad.w));                         /*5*/
                break;
            case 0xE1:  // SBC (Indirect, X)
                Op_IndirectX ();
                Op_Sbc (read_cycle (ad.w));
                break;
            case 0xF1:  // SBC (Indirect), Y
                Op_IndirectY ();
                Op_Sbc (read_cycle (ad.w));
                break;
            

            ///////////////////////////////////////////////////////////////////////
            /// ISB
            ///////////////////////////////////////////////////////////////////////
            case 0xE7: Op_ZeroPage  (); Op_Isb (); break;           // ISB ZeroPage
            case 0xF7: Op_ZeroPageX (); Op_Isb (); break;           // ISB ZeroPage,X
            case 0xEF: Op_Absolute  (); Op_Isb (); break;           // ISB Absolute
            case 0xFF: Op_AbsoluteX (); Op_Isb (); break;           // ISB Absolute,X
            case 0xFB: Op_AbsoluteY (); Op_Isb (); break;           // ISB Absolute,Y                
            case 0xE3: Op_IndirectX (); Op_Isb (); break;           // ISB (Indirect),X
            case 0xF3: Op_IndirectY (); Op_Isb (); break;           // ISB (Indirect,Y)
            
            ///////////////////////////////////////////////////////////////////////
            /// SLO
            ///////////////////////////////////////////////////////////////////////
            case 0x07: Op_ZeroPage  (); Op_Slo (); break;           // SLO ZeroPage
            case 0x17: Op_ZeroPageX (); Op_Slo (); break;           // SLO ZeroPage,X
            case 0x0F: Op_Absolute  (); Op_Slo (); break;           // SLO Absolute
            case 0x1F: Op_AbsoluteX (); Op_Slo (); break;           // SLO Absolute,X
            case 0x1B: Op_AbsoluteY (); Op_Slo (); break;           // SLO Absolute,Y                
            case 0x03: Op_IndirectX (); Op_Slo (); break;           // SLO (Indirect),X
            case 0x13: Op_IndirectY (); Op_Slo (); break;           // SLO (Indirect,Y)

            ///////////////////////////////////////////////////////////////////////
            /// RLA
            ///////////////////////////////////////////////////////////////////////
            case 0x27: Op_ZeroPage  (); Op_Rla (); break;           // RLA ZeroPage
            case 0x37: Op_ZeroPageX (); Op_Rla (); break;           // RLA ZeroPage,X
            case 0x2F: Op_Absolute  (); Op_Rla (); break;           // RLA Absolute
            case 0x3F: Op_AbsoluteX (); Op_Rla (); break;           // RLA Absolute,X
            case 0x3B: Op_AbsoluteY (); Op_Rla (); break;           // RLA Absolute,Y                
            case 0x23: Op_IndirectX (); Op_Rla (); break;           // RLA (Indirect),X
            case 0x33: Op_IndirectY (); Op_Rla (); break;           // RLA (Indirect,Y)

            ///////////////////////////////////////////////////////////////////////
            /// RLA
            ///////////////////////////////////////////////////////////////////////
            case 0x47: Op_ZeroPage  (); Op_Sre (); break;           // SRE ZeroPage
            case 0x57: Op_ZeroPageX (); Op_Sre (); break;           // SRE ZeroPage,X
            case 0x4F: Op_Absolute  (); Op_Sre (); break;           // SRE Asbsolute
            case 0x5F: Op_AbsoluteX (); Op_Sre (); break;           // SRE Absolute,X
            case 0x5B: Op_AbsoluteY (); Op_Sre (); break;           // SRE Absolute,Y                
            case 0x43: Op_IndirectX (); Op_Sre (); break;           // SRE (Indirect),X
            case 0x53: Op_IndirectY (); Op_Sre (); break;           // SRE (Indirect,Y)

            ///////////////////////////////////////////////////////////////////////
            /// RRA
            ///////////////////////////////////////////////////////////////////////
            case 0x67: Op_ZeroPage  (); Op_Rra (); break;           // RRA ZeroPage
            case 0x77: Op_ZeroPageX (); Op_Rra (); break;           // RRA ZeroPage,X
            case 0x6F: Op_Absolute  (); Op_Rra (); break;           // RRA Asbsolute
            case 0x7F: Op_AbsoluteX (); Op_Rra (); break;           // RRA Absolute,X
            case 0x7B: Op_AbsoluteY (); Op_Rra (); break;           // RRA Absolute,Y                
            case 0x63: Op_IndirectX (); Op_Rra (); break;           // RRA (Indirect),X
            case 0x73: Op_IndirectY (); Op_Rra (); break;           // RRA (Indirect,Y)

            ///////////////////////////////////////////////////////////////////////
            /// KIL
            ///////////////////////////////////////////////////////////////////////
            case 0x02: State.Jammed = 1; break; 
            case 0x12: State.Jammed = 1; break; 
            case 0x22: State.Jammed = 1; break; 
            case 0x32: State.Jammed = 1; break; 
            case 0x42: State.Jammed = 1; break; 
            case 0x52: State.Jammed = 1; break; 
            case 0x62: State.Jammed = 1; break; 
            case 0x72: State.Jammed = 1; break; 
            case 0x92: State.Jammed = 1; break; 
            case 0xB2: State.Jammed = 1; break; 
            case 0xD2: State.Jammed = 1; break; 
            case 0xF2: State.Jammed = 1; break;

            default: {
                std::array<char, 256> buffer_;
                std::sprintf (std::data (buffer_), "Bad Opcode : %02X", o);
                throw std::runtime_error (std::data (buffer_));
            }
        }
        State.WriteDisabled = 0;
    }

    template<typename _MachineInterface, typename _BusInterface>
    inline void ricoh_core<_MachineInterface, _BusInterface>::reset () {
        State.Rst = 1;
    }

    template<typename _MachineInterface, typename _BusInterface>
    inline const auto& ricoh_core<_MachineInterface, _BusInterface>::state () const {
        return State;
    }

    template<typename _MachineInterface, typename _BusInterface>
    inline auto& ricoh_core<_MachineInterface, _BusInterface>::state () {
        return State;
    }


}

#endif