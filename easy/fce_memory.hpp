#ifndef __FCE_MEMORY_HPP__
#define __FCE_MEMORY_HPP__

#include "fce_utils.hpp"
#include <array>
namespace libfce {

    template <typename _BusInterface, std::size_t _Base, 
        std::size_t _Last, std::size_t _Size = _Last - _Base>
    struct memory {
        memory () {
            reset ();
        }

        bool cycle (_BusInterface& bus) {
            if (libfce::outside (bus.address (), _Base, _Last)) {
                return false;
            }

            auto& cell_ = Store [(bus.address () - _Base) % _Size];
            if (bus.write ()) {
                cell_ = bus.data () ;
                return true;
            }
            bus.data (cell_);
            return true;
        }

        void reset () {
            std::fill (std::begin (Store), std::end (Store), 0u);
        }

    private:
        std::array<std::uint8_t, _Size> Store;
    };

}


#endif