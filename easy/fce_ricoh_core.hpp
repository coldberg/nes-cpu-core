#ifndef __FCE_RICCOH_CORE_HPP__
#define __FCE_RICCOH_CORE_HPP__


#include <cstddef>
#include <cstdint>
#include <functional>

#include "fce_bits.hpp"

namespace libfce {
        
    template <typename _MachineInterface, typename _BusInterface>
    struct ricoh_core {

        ricoh_core (_MachineInterface&, _BusInterface&);

        void one ();
        void reset ();

        const auto& state () const;
        auto& state ();

    private:        

        static const std::uint16_t _StackVector = 0x0100;
        static const std::uint16_t _NmiVector   = 0xFFFA;
        static const std::uint16_t _ResetVector = 0xFFFC;
        static const std::uint16_t _IrqVector   = 0xFFFE;
        static const std::uint16_t _TestVector  = 0xC000;

        template <std::size_t _Offset, std::size_t _Size>
        using register_bits = bit_field<
            _Offset, _Size, std::uint64_t>;
        std::uint8_t read_cycle (std::uint16_t address);
        std::uint8_t write_cycle (std::uint16_t address, std::uint8_t data);

        union core_state_t {
            std::uint64_t CompleteState;
            register_bits<0, 16> Pc;
            register_bits<0, 8> Pcl;
            register_bits<8, 8> Pch;
            register_bits<16, 8> S;
            register_bits<24, 8> A;
            register_bits<32, 8> X;
            register_bits<40, 8> Y;
            register_bits<48, 8> P;
            register_bits<48, 1> C;
            register_bits<49, 1> Z;
            register_bits<50, 1> I;
            register_bits<51, 1> D;
            register_bits<52, 1> B;
            register_bits<53, 1> _One;            
            register_bits<54, 1> V;
            register_bits<55, 1> N;

            register_bits<56, 3> Signals;

            register_bits<56, 1> Nmi;
            register_bits<57, 1> Rst;
            register_bits<58, 1> Irq;
            register_bits<59, 1> WriteDisabled;
            register_bits<60, 1> FreeBit0;

            register_bits<61, 1> FreeBit1;
            register_bits<62, 1> FreeBit2;

            register_bits<63, 1> Jammed;

            core_state_t (std::uint64_t init_):
                CompleteState (init_)
            {}
        } State;

        _MachineInterface& MachineInterface;
        _BusInterface& BusInterface;
    };
}

#include "fce_ricoh_core_impl.hpp"

#endif

