#ifndef __RICOH_IOLOGIC_HPP__
#define __RICOH_IOLOGIC_HPP__

namespace libfce {
    template <typename _MachineInterface, typename _BusInterface>
    struct ricoh_iologic {
        ricoh_iologic (_MachineInterface& mi, _BusInterface& bi);
        bool cycle (_BusInterface&);

    private:
        _MachineInterface& MachineInterface;
        _BusInterface& BusInterface;
    };

}
#include "fce_ricoh_iologic_impl.hpp"
#endif
